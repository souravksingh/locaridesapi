import simpy, itertools, MySQLdb,json, datetime as dt
from LocaRidesAPINonDistributed import LocaAPIDBTbls as apiDB, LocaMain as Lmain, commonFunction as cf, webConfig as webC

def RunSimulation():
    conn = MySQLdb.connect('mysql-locatest1.cihzsxxugwmt.ap-south-1.rds.amazonaws.com',
                           'dbadmin123', 'dbpass123', 'VRPSampleNonDistributed', autocommit=True,
                           port=3306, use_unicode=True, charset="utf8")
    cursor = conn.cursor()
    try:
        print("StartTime:%s" % dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S:%f"))
        query = "Truncate table CustomerFlows"
        cursor.execute(query)
        query = "Truncate table trip"
        cursor.execute(query)

        env = simpy.Environment()
        LocaObj = Lmain.LocaAPI()
        env.process(customerGetETASimulation(env,LocaObj))
        env.process(customerBookVehicleSimulation(env,LocaObj))
        env.process(customerCancelBookingSimulaton(env,LocaObj))
        for vehID in LocaObj.VMObj.__dict__:
            vehicleDict = LocaObj.VMObj.getValue(vehID)
            env.process(vehicleEndSimulation(env,LocaObj,vehID,vehicleDict))

        env.run(until=55000)
        print("EndTime:%s" % dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S:%f"))

    # except Exception as ex:
    #     print("RunSimulation-Error:",ex)
    finally:
        cursor.close()
        conn.close()

def customerGetETASimulation(env,LocaObj):
    conn = MySQLdb.connect('mysql-locatest1.cihzsxxugwmt.ap-south-1.rds.amazonaws.com',
                           'dbadmin123', 'dbpass123', 'VRPSampleNonDistributed', autocommit=True,
                           port=3306, use_unicode=True, charset="utf8")
    cursor = conn.cursor()
    try:
        session = apiDB.loadSession()

        custData = session.query(apiDB.CustomerDemand).all()
        session.close()

        for cust in custData:

            yield env.timeout(cust.ArrivalTime-env.now)
            webC.logger.debug(
                "at t=%s, API: getETA, input: (%s,%s,1), Extra: CID=%s" % (env.now, cust.PickupPoint,
                                                                        cust.DropPoint, cust.CID))
            tempETA = LocaObj.getETA(cust.PickupPoint, cust.DropPoint, 1)
            webC.logger.debug(
                "at t=%s, API: getETA, Result: %s" % (env.now, tempETA))

            if tempETA != "None":
                query = "insert into CustomerFlows(CID, GetETAResponse) values(%s,%s)"
                cursor.execute(query, (cust.CID,tempETA))
            else:
                query = "insert into CustomerFlows(CID, GetETAResponse) values(%s,%s)"
                cursor.execute(query,(cust.CID,"%s,%s" %(tempETA, "No vehicle to serve request")))

    # except Exception as ex:
    #     print("customerGetETASimulation-Error:",ex)
    finally:
        # conn.commit()
        cursor.close()
        conn.close()

def customerBookVehicleSimulation(env,LocaObj):
    conn = MySQLdb.connect('mysql-locatest1.cihzsxxugwmt.ap-south-1.rds.amazonaws.com',
                           'dbadmin123', 'dbpass123', 'VRPSampleNonDistributed', autocommit=True,
                           port=3306, use_unicode=True, charset="utf8")
    cursor = conn.cursor()
    try:
        query = """Select CID, (ArrivalTime+BookedAt) as BookedAt, 
                        PickupPoint, DropPoint from CustomerDemand 
                        order by (ArrivalTime+BookedAt)"""
        cursor.execute(query)
        custData = [i for i in cursor]
        for cust in custData:

            yield env.timeout(cust[1]-env.now)
            webC.logger.debug(
                "at t=%s, API: BookVehicle, input: (%s,%s,%s,1)" % (env.now, cust[2],cust[3],cust[0]))
            vehicleID, tempETA, rerouteFlag = LocaObj.BookVehicle(cust[2],cust[3],cust[0],1)
            webC.logger.debug(
                "at t=%s, API: BookVehicle, Result: (%s,%s,%s)" % (env.now, vehicleID,tempETA,rerouteFlag))
            if vehicleID != "None":
                query = """update CustomerFlows set VehicleName=%s, BookAPIResponse=%s, RequestStatus='Booked' where CID=%s"""
                cursor.execute(query, (vehicleID,"%s,%s" %(vehicleID,tempETA), cust[0]))
                if rerouteFlag > 0:
                    vehicleDict = LocaObj.VMObj.getValue(vehicleID)
                    query = """insert into trip(VehicleID, StartTime, SourceDepot, 
                                                DestinationDepot, TotalCapacity, CurrentCapacity, DestinationDropPoints,
                                                Distance, TimeTaken, InitialLocation, Message) values(%s, %s, %s,
                                                %s, %s, %s, %s, %s, %s, %s, %s)"""
                    cursor.execute(query, (vehicleID, env.now, vehicleDict["LastSite"], vehicleDict["DropPoints"][0],
                                           vehicleDict["SeatingCaps"],
                                           vehicleDict["SeatingCaps"] - vehicleDict["CurrSeated"],
                                           "->".join(vehicleDict["DropPoints"]),
                                           LocaObj.distance_calculator([vehicleDict["LastSite"], vehicleDict["DropPoints"][0]]),
                                           LocaObj.time_calculator([vehicleDict["LastSite"], vehicleDict["DropPoints"][0]]),
                                           vehicleDict["HomeSite"], "Re-routing due to new pickup arrival"))

            else:
                query = """update CustomerFlows set BookAPIResponse=%s, RequestStatus='Failed' where CID=%s"""
                cursor.execute(query,("No vehicle to serve request", cust[0]))

    # except Exception as ex:
    #     print("customerBookVehicleSimulation-Error:",ex,cust[0])
    finally:

        cursor.close()
        conn.close()

def vehicleEndSimulation(env, LocaObj, vehicleID, vehicleDict):
    conn = MySQLdb.connect('mysql-locatest1.cihzsxxugwmt.ap-south-1.rds.amazonaws.com',
                           'dbadmin123', 'dbpass123', 'VRPSampleNonDistributed', autocommit=True,
                           port=3306, use_unicode=True, charset="utf8")
    cursor = conn.cursor()
    try:
        homeAtLoca = vehicleDict["HomeSite"]
        towardOrAtLoca = homeAtLoca

        for i in itertools.count():

            # if vehicleID == 'V16':
            #     print("Test-case at %s" %env.now)
            if vehicleDict["ETA"] == 0:
                tempCurrDropCount = vehicleDict["DropCount"][0]

                if len(tempCurrDropCount) > 0:
                    for cust in tempCurrDropCount:

                        query = """update CustomerFlows set RequestStatus='Processed',
                                ActualDropTime=%s, ActualTravelTime=(%s-ActualPickTime) where CID=%s 
                                and vehicleName='%s' and RequestStatus='Boarded'""" %(env.now,env.now,cust,
                                                                                   vehicleID)
                        cursor.execute(query)

                tempWaitTime = LocaObj.SMObj.getValue(towardOrAtLoca)["WaitTime"]
                yield env.timeout(tempWaitTime)

                if len(vehicleDict["DropPoints"]) == 1 and vehicleDict["DropPoints"][0] != homeAtLoca:
                    # print("at %s vehicle %s waiting till maxIdleTime" %(env.now,vehicleID))
                    yield env.timeout(LocaObj.SPObj.getValue("MaxIdleTime"))

                tempCurrPickCount = vehicleDict["PickCount"][0]
                if len(tempCurrPickCount) > 0:
                    for cust in tempCurrPickCount:
                        # if cust in [53]:
                        #     print("Testing")
                        # if vehicleID in ['V2'] and cust in [16]:
                        #     print("Testing")
                        tempExpectedTTime = LocaObj.time_calculator(vehicleDict["DropPoints"][0:
                                            [True if cust in z else False for z in vehicleDict["DropCount"]].index(True)+1])
                        query = """update CustomerFlows set RequestStatus='Boarded',
                                ActualPickTime=%s, ExpectedTravelTime=%s where CID=%s 
                                and vehicleName='%s' and RequestStatus='Booked'""" %(env.now,tempExpectedTTime,cust,
                                                                                   vehicleID)
                        cursor.execute(query)

                query = """Select DestinationDepot 
                            from trip 
                            where VehicleID = %s 
                            and TripID in (Select max(TripID) 
                                                from trip 
                                                where VehicleID = %s)"""
                cursor.execute(query,(vehicleID,vehicleID))
                towardOrAtLoca1 = cursor.fetchone()
                if towardOrAtLoca1 is not None:
                    if towardOrAtLoca1[0] != towardOrAtLoca:
                        towardOrAtLoca = towardOrAtLoca1[0]


                if towardOrAtLoca == vehicleDict["HomeSite"] and \
                    sum(len(i) for i in vehicleDict["DropCount"]) == 0 and \
                        sum(len(i) for i in vehicleDict["PickCount"]) == 0:
                    tempNextStop = vehicleDict["HomeSite"]
                else:
                    tempNextStop = LocaObj.GetNextStop(vehicleID,towardOrAtLoca,env)

                if (tempNextStop != towardOrAtLoca and tempNextStop != "You can have your break now") \
                        or len(tempCurrDropCount)+len(tempCurrPickCount) > 0:
                    query = """insert into trip(VehicleID, StartTime, SourceDepot, 
                            DestinationDepot, TotalCapacity, CurrentCapacity, DestinationDropPoints,
                            PickCount, DropCount, Distance, TimeTaken, InitialLocation) values(%s, %s, %s,
                            %s, %s, %s, %s, %s, %s, %s, %s, %s)"""
                    cursor.execute(query,(vehicleID,env.now,towardOrAtLoca,tempNextStop,
                                          vehicleDict["SeatingCaps"],
                                          vehicleDict["SeatingCaps"]-vehicleDict["CurrSeated"],
                                          "->".join(vehicleDict["DropPoints"]),
                                          json.dumps(tempCurrPickCount),
                                          json.dumps(tempCurrDropCount),
                                          LocaObj.distance_calculator([towardOrAtLoca,tempNextStop]),
                                          LocaObj.time_calculator([towardOrAtLoca, tempNextStop]),
                                          vehicleDict["HomeSite"]))
                    # print("vehicle %s arrived at %s" %(vehicleID,towardOrAtLoca))
                    vehicleDict["ETA"] = LocaObj.time_calculator([towardOrAtLoca,tempNextStop])
                    towardOrAtLoca = tempNextStop
                    # print("vehicle %s left for %s" %(vehicleID,tempNextStop))
                elif tempNextStop == towardOrAtLoca:
                    vehicleDict["ETA"] = 0
                    towardOrAtLoca = tempNextStop
                else:   #This case is for vehicle break start
                    query = """insert into trip(VehicleID, StartTime, SourceDepot, 
                                                DestinationDepot, TotalCapacity, CurrentCapacity, 
                                                DestinationDropPoints, PickCount, DropCount, 
                                                InitialLocation, Message) values(%s, %s, %s,
                                                %s, %s, %s, %s, %s, %s, %s, %s)"""
                    cursor.execute(query, (vehicleID, env.now, towardOrAtLoca, "NA",vehicleDict["SeatingCaps"],
                                          vehicleDict["SeatingCaps"]-vehicleDict["CurrSeated"],"NA",
                                           json.dumps(tempCurrPickCount), json.dumps(tempCurrDropCount),
                                           vehicleDict["HomeSite"], tempNextStop))
                    return None

            elif 0 < vehicleDict["ETA"] <= webC.ETARefreshVal:
                yield env.timeout(vehicleDict["ETA"])
                vehicleDict["ETA"] = 0
                dstCords = LocaObj.SMObj.getValue(vehicleDict["DropPoints"][0])
                vehicleDict["Cords"] = dstCords["SiteLat"],dstCords["SiteLong"]
            else:
                yield env.timeout(webC.ETARefreshVal)
                if towardOrAtLoca != vehicleDict["DropPoints"][0]:
                    towardOrAtLoca = vehicleDict["DropPoints"][0]
                vehicleDict["ETA"] -= webC.ETARefreshVal
                srcCords = LocaObj.SMObj.getValue(vehicleDict["LastSite"])
                srcCords = srcCords["SiteLat"],srcCords["SiteLong"]
                dstCords = LocaObj.SMObj.getValue(vehicleDict["DropPoints"][0])
                dstCords = dstCords["SiteLat"],dstCords["SiteLong"]
                ttlTime = LocaObj.DTDObj.getValue("%s-%s" %(vehicleDict["LastSite"],vehicleDict["DropPoints"][0]))["Time"]
                vehicleDict["Cords"] = cf.getCordsUsingETAs(srcCords,dstCords,ttlTime,vehicleDict["ETA"])

    # except Exception as ex:
    #     print("vehicleEndSimulation-Error:",ex)
    finally:
        cursor.close()
        conn.close()

def customerCancelBookingSimulaton(env,LocaObj):
    conn = MySQLdb.connect('mysql-locatest1.cihzsxxugwmt.ap-south-1.rds.amazonaws.com',
                           'dbadmin123', 'dbpass123', 'VRPSampleNonDistributed', autocommit=True,
                           port=3306, use_unicode=True, charset="utf8")
    cursor = conn.cursor()
    try:
        query = """select CID,(ArrivalTime+BookedAt+CancelAt) as CancelAt from CustomerDemand 
                where CancelAt > 0 order by (ArrivalTime+BookedAt+CancelAt)"""
        cursor.execute(query)

        for cust in cursor:
            yield env.timeout(cust[1] - env.now)
            # if env.now > 3017:
            #     print("Testing")
            query = """Select RequestStatus,VehicleName from CustomerFlows where CID=%s""" %cust[0]

            cursor.execute(query)
            tempRS,tempVehName = cursor.fetchone()
            if tempRS == "Booked":
                resp = LocaObj.cancelBooking(cust[0],tempVehName)
                if resp == "Booking Cancelled":
                    print("Customer %s cancelled booking at %s" %(cust[0],env.now))
                    query = """Update CustomerFlows set CancelBookingResponse='%s',
                                 RequestStatus = 'Cancelled'
                                where CID=%s and VehicleName='%s'
                                and RequestStatus = 'Booked'""" %("called at %s" %env.now,cust[0],tempVehName)
                    cursor.execute(query)

    # except Exception as ex:
    #     print("customerCancelBookingSimulaton-Error:", ex)
    finally:
        cursor.close()
        conn.close()

def vehicleBreakStartSimulation(env,LocaObj):
    conn = MySQLdb.connect('mysql-locatest1.cihzsxxugwmt.ap-south-1.rds.amazonaws.com',
                           'dbadmin123', 'dbpass123', 'VRPSampleNonDistributed', autocommit=True,
                           port=3306, use_unicode=True, charset="utf8")
    cursor = conn.cursor()
    try:
        query = """select VehicleID, BreakStartAt from vehicle 
                where BreakStartAt is not null order by BreakStartAt"""

        cursor.execute(query)

        for veh in cursor:
            yield env.timeout(veh[1] - env.now)
            LocaObj.DriverBreakStart(veh[0])
            print("Vehicle %s Called for break at %s" %(veh[0],env.now))

    except Exception as ex:
        print("vehicleBreakStartSimulation-Error:", ex)
    finally:
        cursor.close()
        conn.close()

def vehicleBreakEndSimulation(env,LocaObj):
    conn = MySQLdb.connect('mysql-locatest1.cihzsxxugwmt.ap-south-1.rds.amazonaws.com',
                           'dbadmin123', 'dbpass123', 'VRPSampleNonDistributed', autocommit=True,
                           port=3306, use_unicode=True, charset="utf8")
    cursor = conn.cursor()
    try:
        query = """select VehicleID, BreakEndAt, HomeStatn from vehicle 
                where BreakEndAt is not null order by BreakEndAt"""

        cursor.execute(query)

        for veh in cursor:
            yield env.timeout(veh[1] - env.now)
            LocaObj.DriverBreadEnd(veh[0], veh[2])
            print("Vehicle %s ended the break at %s" %(veh[0],env.now))
            query = """insert into trip(VehicleID, StartTime, SourceDepot, 
                       Message) values(%s, %s, %s, %s)"""
            cursor.execute(query, (veh[0], env.now, veh[2], "Back To work at %s" %env.now))

    except Exception as ex:
        print("vehicleBreakEndSimulation-Error:", ex)
    finally:
        cursor.close()
        conn.close()

RunSimulation()