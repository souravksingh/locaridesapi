

class Vehicle_Matrix(dict):

    def setValue(self,key, dataDict):
        """

        :param key: this must be a vehicle id from database
        :param dataDict: this must be a dict of the following format
                        {"SeatingCaps":0, "CurrSeated":0, "HomeSite":"", "DropPoints": [], "DropCount": [[]],
                        "PickCount":[[]],"MaxDetourTime":[10000],"ETA":0,"MaxRoamTime":0,
                        "VehicleType":"", "Status":"Active", "Cords":(), "LastSite":""}
        :return: None
        """
        self.__dict__[key] = dataDict

    def getValue(self,key):
        """

        :param key: this must be a vehicle id from database
        :return: Dictionary type value containing information regarding the vehicle id
        """
        return self.__dict__[key]

class Site_Matrix(dict):

    def setValue(self,key, dataDict):
        """

        :param key: this must be a Site id/Site name from database
        :param dataDict: this must be a dict of the following format
                        {"WaitTime":0, "MaxServeTime":0, "PastPickup": {},
                        "SiteLat":0, "SiteLong":0}
        :return: None
        """
        self.__dict__[key] = dataDict

    def getValue(self,key):
        """

        :param key: this must be a Site id/Site name from database
        :return: Dictionary type value containing information regarding the vehicle id
        """
        return self.__dict__[key]

class DistanceTimeData(dict):

    def setValue(self,key, dataDict):
        """

        :param key: this must be a Site name combination separated by '-'
        :param dataDict: this must be a dict of the following format
                        {"Distance":0, "Time":0, "MTV":0, "PossRoutes":[[],[]]}
        :return: None
        """
        self.__dict__[key] = dataDict

    def getValue(self,key):
        """

        :param key: this must be a Site name combination separated by '-'
        :return: Dictionary type value containing information regarding the two Site combination
        """
        return self.__dict__[key]

class SetupParameters(dict):

    def setValue(self,key, value):
        """

        :param key: this must be a parameter name combination
        :param value: this must be a integer

        :return: None
        """
        self.__dict__[key] = value

    def getValue(self,key):
        """

        :param key: this must be a Site name combination separated by '-'
        :return: integer type value containing information regarding the parameter name
        """
        return self.__dict__[key]