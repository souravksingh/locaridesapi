import sqlalchemy
from sqlalchemy import create_engine
from sqlalchemy.engine.url import URL
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Enum, Float, DateTime
from sqlalchemy.orm import sessionmaker
import enum
import datetime


engine = create_engine(URL ('mysql',
                            host = 'mysql-locatest1.cihzsxxugwmt.ap-south-1.rds.amazonaws.com',
                            username = 'dbadmin123',
                            password = 'dbpass123',
                            database = 'sampledb'))

Base = declarative_base()
Session = sessionmaker()
Session.configure(bind=engine)

class Customer(Base):
    __tablename__ = 'customers'

    phone_num = Column(String(64), primary_key = True)
    name = Column(String(255), nullable=False)
    otp = Column(Integer)
    validated = Column(Integer)
    wallet_id = Column(String(255))
    pass_id = Column(Integer)
    device_id = Column(String(255))

    def __repr__(self):
        return "<Customer(phone_num='%s', name='%s', otp='%d', validate='%d', wallet_id='%s', pass_id='%d', device_id='%s')>" % (self.phone_num, self.name, self.otp, self.validated, self.wallet_id, self.pass_id, self.device_id)


    @staticmethod
    def get(phnum):
        session = Session()
        cuser = session.query(Customer).filter_by(phone_num=phnum).first()
        session.close()
        return cuser

    @staticmethod
    def add(cust):
        session = Session()
        session.add(cust)
        session.commit()
        session.close()

class Status(enum.Enum):
    out_of_service = 0
    stopped = 1
    running = 2

class Routing(enum.Enum):
    static = 0
    semi_dynamic = 1
    dynamic = 2


class Vehicle(Base):
    __tablename__ = 'vehicles'

    id = Column(Integer, primary_key = True)
    reg_number = Column(String(64), nullable=False)
    capacity = Column(Integer, nullable=False)
    status = Column(Integer, nullable=False)
    home_station = Column(Integer, nullable=False)
    routing = Column(Integer, nullable=False)
    latitude = Column(Float)
    longitude = Column(Float)

    @staticmethod
    def get_vehicle(qid):
        session = Session()
        veh = session.query(Vehicle).filter_by(id = qid).first()
        session.close()
        return veh

    @staticmethod
    def add_vehicle(veh):
        session = Session()
        session.add(veh)
        session.commit()
        session.close()

    @staticmethod
    def get_vehicle_location(vehicle_id):
        session = Session()
        result = None
        veh = session.query(Vehicle).filter_by(id = vehicle_id).first()
        session.close()
        if veh is None:
            raise('Vehicle id is invalid')
        else:
            result = (veh.latitude, veh.longitude)
            return result

    @staticmethod
    def get_home_station(vehicle_id):
        session = Session()
        veh = session.query(Vehicle).filter_by(id = vehicle_id).first()
        session.close()
        if veh is None:
            raise('Vehicle id is invalid')
        else:
            result = veh.home_station
            return result

    def __repr__(self):
        return "<Vehicle(id='%s', reg_number='%s', capacity='%d', status='%d', home_statio='%d', routing='%d')>" % (self.id, self.reg_number, self.capacity, self.status, self.home_station, self.routing)

class Point(enum.Enum):
    regular = 0
    endpoint = 1

class LocaPoint(Base):
    __tablename__ = 'locapoints'

    id = Column(Integer, primary_key = True)
    name = Column(String(255), nullable=False)
    point_type = Column(Enum(Point))
    latitude = Column(Float)
    longitude = Column(Float)
    max_serve_time = Column(Integer)

    def __repr__(self):
        return "<LocaPoint(id='%d', name='%s', point_type='%d',latitude='%f', longitude='%f', max_serve_time='%d'>" % (self.id, self.name, self.point_type, self.latitude, self.longitude, self.max_serve_time)

class OSType(enum.Enum):
    iOS = 0
    Android = 1

class Device(Base):
    __tablename__ = 'devices'

    id = Column(String(255), primary_key=True)
    type = Column(Enum(OSType))
    push_key = Column(String(255))
    manufacturer = Column(String(255))
    model = Column(String(255))
    version = Column(String(64))

    def __repr__(self):
        return "<Device(id='%s', type='%d', push_key='%s', manufacturer='%s', model='%s', version='%s'>" % (self.id, self.type, self.push_key, self.manufacturer, self.model,self.version)

class Wallet(Base):
    __tablename__ = 'wallets'

    id = Column(Integer, primary_key = True)
    balance = Column(Integer)
    expiry = Column(DateTime)
    expire_amount = Column(Integer)

    def __repr__(self):
        return "<Wallet(id='%d', balance='%d', expiry='%s', expire_amount='%d'>" % (self.id, self.balance, str(self.expiry), self.expire_amount)

class PassType(enum.Enum):
    weekly = 0
    monthly = 1

class Pass(Base):
    __tablename__ = 'passes'

    id = Column(Integer, primary_key = True)
    type = Column(Enum(PassType))
    expiry = Column(DateTime)
    auto_renew = Column(Integer)
    trips_remaining = Column(Integer)

    def __repr__(self):
        return "<Pass(id='%d', type='%d', expiry='%s', auto_renew='%d', trips_remaining='%d'>", (self.id, self.type, str(self.expiry), self.auto_renew, self.trips_remaining)

def init():
    Base.metadata.create_all(engine)

def init_vehicles():
    if not engine.dialect.has_table(engine, "vehicles"):
        Base.metadata.tables["vehicles"].create(bind=engine)

#Base.metadata.create_all(engine)

