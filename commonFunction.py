import math, json, MySQLdb, itertools, copy
from urllib.request import urlopen
from LocaRidesAPINonDistributed import webConfig as webC


class LocaAPIError(BaseException):
    """Loca API defined Errors"""
    def __init__(self,message):
        self.message = message
        super(LocaAPIError,self).__init__(message)


def airDistance(lat1, lon1, lat2, lon2, unit):
    """ Function to Calculate the Air-Distance in KM between two point on earth """

    try:
        theta = lon1 - lon2
        dist = math.sin(deg2rad(lat1)) * math.sin(deg2rad(lat2)) + math.cos(deg2rad(lat1)) * \
                math.cos(deg2rad(lat2)) * math.cos(deg2rad(theta))
        dist = rad2deg(math.acos(dist)) * 60 * 1.1515

        if unit == 'K':
            dist = dist * 1.609344

        elif unit == 'M':
            dist = dist * 1.609344 * 1000

        return round(dist,2)

    except Exception as ex:

        return 0

def deg2rad(deg):       #This function converts decimal degrees to radians
    return float(deg * math.pi/180.0)

def rad2deg(rad):       #This function converts radians to decimal degrees
    return float(rad / math.pi * 180.0)

def googleAPIDistance(origin, destination, destCount, keyString):

    urlLink = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=%s&destinations=%s&mode=driving&key=%s" %(origin, destination, keyString)

    try:
        response = urlopen(urlLink).read()
        data = json.loads(response)
        resulta = ""

        for i in range(0, destCount):

            if str(data["status"]).upper() == "OK":

                if str(data["rows"][0]["elements"][i]["status"]).upper() == "OK":

                    resulta = resulta + str((data["rows"][0]["elements"][i]["distance"]["value"])) + "|"

                else:
                    resulta = resulta + str(data["rows"][0]["elements"][i]["status"]) + "|"

            else:
                resulta = resulta + str(data["status"]) + "|"

        resulta = resulta[0:len(resulta)-1]
        distance = resulta

        return distance

    except Exception as ex:
        return "Error:"+str(ex)

def googleAPITimeGap(origin, destination, destCount, keyString):

    urlLink = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=%s&destinations=%s&mode=driving&key=%s" %(origin, destination, keyString)

    try:
        response = urlopen(urlLink).read()
        data = json.loads(response)
        resulta = ""

        for i in range(0, destCount):

            if str(data["status"]).upper() == "OK":

                if str(data["rows"][0]["elements"][i]["status"]).upper() == "OK":

                    resulta = resulta + str(data["rows"][0]["elements"][i]["duration"]["value"]) + "|"

                else:
                    resulta = resulta + str(data["rows"][0]["elements"][i]["status"]) + "|"

            else:
                resulta = resulta + str(data["status"]) + "|"

        resulta = resulta[0:len(resulta)-1]

        return resulta

    except Exception as ex:
        return "Error:"+str(ex)

def getETAMethod(PickDropList, LocaAPIObj, sim=0):
    """

    :param PickDropList:
    :param LocaAPIObj:
    :param sim:
    :return:
    """

    # if PickDropList == ["Z2P2","Z0P4"]:
    #     print("Testing")
    webC.logger.debug("=============Inside-API: getETA")
    vehicle_matrix = LocaAPIObj.VMObj.__dict__
    poss_ETA = []
    for x in vehicle_matrix:

        if vehicle_matrix[x]["Status"] != "Active":
            continue

        vehicleCords = LocaAPIObj.getVehicleCurrCords(x,sim=sim)
        PickUPCords = LocaAPIObj.SMObj.getValue(PickDropList[0])

        if airDistance(vehicleCords[0],vehicleCords[1],PickUPCords["SiteLat"], PickUPCords["SiteLong"], 'M') > \
            LocaAPIObj.SPObj.getValue("ThresholdDistanceETA"):
            continue

        tempAvailSeats = vehicle_matrix[x]['SeatingCaps'] - vehicle_matrix[x]['CurrSeated']
        tempVehicleETA = LocaAPIObj.VMObj.getValue(x)["ETA"]
        routeDynamic = PickDropList
        routeStatic = vehicle_matrix[x]['DropPoints']
        currPickCount = vehicle_matrix[x]['PickCount']
        currDropCount = vehicle_matrix[x]['DropCount']
        init_route_dist = LocaAPIObj.time_calculator(routeStatic)
        max_detour = vehicle_matrix[x]['MaxDetourTime']
        vehicleType = vehicle_matrix[x]['VehicleType']

        if vehicleType in ["static", "semiDynamic"] and \
                (PickDropList[0] not in routeStatic or PickDropList[1] not in routeStatic):
            continue
        elif vehicleType in ["static", "semiDynamic"] and routeStatic.index(PickDropList[0]) > routeStatic.index(
                PickDropList[1]):
            continue
        elif vehicleType in ["static", "semiDynamic"] and \
                (LocaAPIObj.time_calculator(routeStatic[:routeStatic.index(PickDropList[0])+1]) >
                        LocaAPIObj.SMObj.getValue(PickDropList[0])["MaxServeTime"] or
            (tempAvailSeats + sum(len(a) for a in vehicle_matrix[x]['DropCount'][:routeStatic.index(PickDropList[0])]) - sum(
                len(a) for a in vehicle_matrix[x]['PickCount'][:routeStatic.index(PickDropList[0])])) == 0):
            continue
        elif vehicleType in ["static", "semiDynamic"]:
            if sim == 0:
                tempVehicleETA = LocaAPIObj.getETAUsingGAPI(x) + LocaAPIObj.g_time_calculator(
                    routeStatic[:routeStatic.index(PickDropList[0])+1])
            else:
                tempVehicleETA = LocaAPIObj.VMObj.getValue(x)["ETA"] + LocaAPIObj.time_calculator(
                    routeStatic[:routeStatic.index(PickDropList[0])+1])
            webC.logger.debug("=============Inside-API: case:1,vehicle:%s, ETA:%s" %(x,tempVehicleETA))
            poss_ETA.append(tempVehicleETA)


        if PickDropList[0] in routeStatic and \
            LocaAPIObj.time_calculator(routeStatic[:routeStatic.index(PickDropList[0])+1]) <= \
            LocaAPIObj.SMObj.getValue(PickDropList[0])["MaxServeTime"] and \
                (tempAvailSeats + sum(
                    len(a) for a in vehicle_matrix[x]['DropCount'][:routeStatic.index(PickDropList[0])]) - sum(
                    len(a) for a in vehicle_matrix[x]['PickCount'][:routeStatic.index(PickDropList[0])])) > 0:
            if sim == 0:
                tempVehicleETA = LocaAPIObj.getETAUsingGAPI(x) + LocaAPIObj.g_time_calculator(
                    routeStatic[:routeStatic.index(PickDropList[0])+1])
            else:
                tempVehicleETA = LocaAPIObj.VMObj.getValue(x)["ETA"] + LocaAPIObj.time_calculator(
                    routeStatic[:routeStatic.index(PickDropList[0])+1])
            webC.logger.debug("=============Inside-API: case:2,vehicle:%s, ETA:%s" % (x, tempVehicleETA))
            poss_ETA.append(tempVehicleETA)
        else:

            vehCords = LocaAPIObj.getVehicleCurrCords(x,sim=sim)
            pickCords = LocaAPIObj.SMObj.getValue(PickDropList[0])
            nextLocCords = LocaAPIObj.SMObj.getValue(routeStatic[0])
            x1 = airDistance(vehCords[0], vehCords[1], pickCords["SiteLat"], pickCords["SiteLong"], 'M')
            x2 = LocaAPIObj.distance_calculator([PickDropList[0],routeStatic[0]])
            x3 = airDistance(vehCords[0], vehCords[1], nextLocCords["SiteLat"], nextLocCords["SiteLong"], 'M')

            if ((x1+x2-x3)*3600)/(1000*LocaAPIObj.SPObj.getValue("RadialDetourSpeedKMPH")) <= min(max_detour) and  \
                    tempAvailSeats + len(currDropCount[0]) - len(currPickCount[0]) > 0:
                if sim == 0:
                    tempVehicleETA = googleAPITimeGap("%s,%s" % (vehCords[0], vehCords[1]),
                                                 "%s,%s" % (pickCords["SiteLat"], pickCords["SiteLong"]), 1,
                                                 "AIzaSyDGWX4OefdEbrGeqcfC2ghiWHxPMF8w1tQSourav")
                else:
                    if tempVehicleETA > 0 and x3 > 0:
                        # if x3 == 0:
                        #     print(tempVehicleETA)
                        tempVehicleETA = (tempVehicleETA / x3) * x1

                    else:
                        tempVehicleETA = LocaAPIObj.time_calculator_without_wait([routeStatic[0],PickDropList[0]])
                webC.logger.debug("=============Inside-API: case:3,vehicle:%s, ETA:%s" % (x, tempVehicleETA))
                poss_ETA.append(tempVehicleETA)
            elif tempAvailSeats == 0:
                # print(routeStatic,PickDropList)
                t2 = LocaAPIObj.time_calculator([routeStatic[0], PickDropList[0]])
                x1,x2 = 0,0
                if len(routeStatic) > 1:
                    x1 = LocaAPIObj.time_calculator([routeStatic[0], routeStatic[1]])
                    x2 = LocaAPIObj.time_calculator([PickDropList[0], routeStatic[1]])

                detourMin = min(max_detour[1:]+[10000])
                if (tempAvailSeats + len(currDropCount[0]) - len(currPickCount[0])) > 0 and t2 + x2 - x1 <= detourMin:

                    t1 = tempVehicleETA
                    if sim == 0:

                        t1 = LocaAPIObj.getETAUsingGAPI(x)

                    if (t1+t2) <= LocaAPIObj.SMObj.getValue(PickDropList[0])["MaxServeTime"]:

                        if sim == 0:
                            tempVehicleETA = LocaAPIObj.getETAUsingGAPI(x) + t2
                        else:
                            tempVehicleETA = LocaAPIObj.VMObj.getValue(x)["ETA"] + t2
                        webC.logger.debug("=============Inside-API: case:4,vehicle:%s, ETA:%s" % (x, tempVehicleETA))
                        poss_ETA.append(tempVehicleETA)

                    else:
                        continue
                else:
                    continue

    if len(poss_ETA) > 0:
        return min(poss_ETA)
    else:
        return "None"

def assignVehicle(PickDropList, LocaAPIObj, customerId, sim=0):
    # if PickDropList == ["Z0P2","Z3P14"] or PickDropList == ["Z0P2","Z3P15"]:
    #     print("Testing",customerId)
    # if customerId in [2]:
    #     print("Testing")
    vehicle_matrix = LocaAPIObj.VMObj.__dict__
    tempVehETADict = {}         #in case of vehicle re-routing to new loca point use this ETA
    possible_routes = {}
    poss_rout_dist = {}
    TTA = 0
    customerAssigned = False
    MTV = max([(LocaAPIObj.DTDObj.getValue("%s-%s" %tuple(PickDropList))["Time"]*LocaAPIObj.SPObj.getValue("DetourPercentage")/100),
               LocaAPIObj.SPObj.getValue("DetourMinTime")])
    webC.logger.debug("=============Inside-API: BookVehicle")
    for x in vehicle_matrix:

        if str(vehicle_matrix[x]["Status"]).upper() != "ACTIVE":
            continue

        vehicleCords = LocaAPIObj.getVehicleCurrCords(x, sim=sim)
        PickUPCords = LocaAPIObj.SMObj.getValue(PickDropList[0])

        webC.logger.debug("=============Inside-API: Vehicle %s info: %s" % (x, vehicle_matrix[x]))

        if airDistance(vehicleCords[0], vehicleCords[1], PickUPCords["SiteLat"], PickUPCords["SiteLong"], 'M') > \
                LocaAPIObj.SPObj.getValue("ThresholdDistanceETA"):
            webC.logger.debug("=============Inside-API: Vehicle %s ignored at case 1" %x)
            continue

        pickCords = LocaAPIObj.SMObj.getValue(PickDropList[0])
        dropCords = LocaAPIObj.SMObj.getValue(PickDropList[1])
        routeStatic = vehicle_matrix[x]['DropPoints']
        tempVehicleETA = LocaAPIObj.VMObj.getValue(x)["ETA"]
        vehiLastDestCord = LocaAPIObj.SMObj.getValue(routeStatic[-1])

        tempPD = airDistance(pickCords["SiteLat"], pickCords["SiteLong"], dropCords["SiteLat"],
                             dropCords["SiteLong"], 'M')
        tempPV = airDistance(pickCords["SiteLat"], pickCords["SiteLong"],
                             vehiLastDestCord["SiteLat"], vehiLastDestCord["SiteLong"], 'M')
        tempDV = airDistance(vehiLastDestCord["SiteLat"], vehiLastDestCord["SiteLong"],
                             dropCords["SiteLat"], dropCords["SiteLong"], 'M')

        if tempPV > 0:
            # print(tempPD,tempPV,tempDV)
            tempTAA = (pow(tempPD, 2) + pow(tempPV,2) - pow(tempDV,2)) / (2 * tempPD * tempPV)
            tempTAA = math.acos(round(tempTAA,4)) * 180 / math.acos(
                -1)
        else:
            tempTAA = 0

        if tempTAA > LocaAPIObj.SPObj.getValue("ThresholdAngle"):
            webC.logger.debug("=============Inside-API: Vehicle %s ignored at case 2" % x)
            continue

        tempAvailSeats = vehicle_matrix[x]['SeatingCaps'] - vehicle_matrix[x]['CurrSeated']

        if tempAvailSeats == 0:
            webC.logger.debug("=============Inside-API: Vehicle %s ignored at case 3" % x)
            continue

        routeDynamic = PickDropList
        # currPickCount = vehicle_matrix[x]['PickCount']
        init_route_dist = LocaAPIObj.time_calculator(routeStatic)
        max_detour = vehicle_matrix[x]['MaxDetourTime']
        vehicleType = vehicle_matrix[x]['VehicleType']
        currPickList = vehicle_matrix[x]["PickCount"]
        currDropList = vehicle_matrix[x]["DropCount"]

        if vehicleType in ["static", "semiDynamic"] and \
                (PickDropList[0] not in routeStatic or PickDropList[1] not in routeStatic):
            webC.logger.debug("=============Inside-API: Vehicle %s ignored at case 4" % x)
            continue
        else:
            if vehicleType in ["static", "semiDynamic"] and routeStatic.index(PickDropList[0]) > routeStatic.index(PickDropList[1]):
                webC.logger.debug("=============Inside-API: Vehicle %s ignored at case 5" % x)
                continue

        if vehicleType in ["semiDynamic1"] and PickDropList[1] not in vehicle_matrix[x]["PossibleDrops"]:
            webC.logger.debug("=============Inside-API: Vehicle %s ignored at case 6" % x)
            continue


        if len(routeStatic) == 0:
            routeStatic = [vehicle_matrix[x]['HomeSite']]

        each_poss_routes = []
        crupt_Indx = []

        if vehicleType in ["static", "semiDynamic"] and \
            tempVehicleETA + LocaAPIObj.SMObj.getValue(routeStatic[0])["WaitTime"] + \
                LocaAPIObj.time_calculator(routeStatic[:routeStatic.index(routeDynamic[0])]) \
                <= LocaAPIObj.SMObj.getValue(routeDynamic[0])["MaxServeTime"]:
            each_poss_routes.append(routeStatic)
        else:
            dropIndx = 50
            if PickDropList[1] in routeStatic:
                dropIndx = routeStatic.index(PickDropList[1])
            if PickDropList[0] != routeStatic[0] and tempVehicleETA > LocaAPIObj.SPObj.getValue("ThresholdMinimumETA"):

                i = 0
                vehCords = LocaAPIObj.getVehicleCurrCords(x, sim=sim)
                nextLocCords = LocaAPIObj.SMObj.getValue(routeStatic[0])
                x1 = airDistance(vehCords[0], vehCords[1], pickCords["SiteLat"], pickCords["SiteLong"], 'M')
                x2 = airDistance(pickCords["SiteLat"], pickCords["SiteLong"],
                                 nextLocCords["SiteLat"], nextLocCords["SiteLong"], 'M')#LocaAPIObj.distance_calculator([PickDropList[0], routeStatic[0]])
                x3 = airDistance(vehCords[0], vehCords[1], nextLocCords["SiteLat"], nextLocCords["SiteLong"], 'M')



                if ((x1+x2-x3)*3600)/(1000*LocaAPIObj.SPObj.getValue("RadialDetourSpeedKMPH")) \
                        + LocaAPIObj.SMObj.getValue(PickDropList[0])["WaitTime"] <= min(max_detour) \
                        and tempAvailSeats > 0:
                    if sim == 0:
                        tempVehETA = googleAPITimeGap("%s,%s" % (vehCords[0], vehCords[1]),
                                                          "%s,%s" % (pickCords["SiteLat"], pickCords["SiteLong"]),
                                                          1,
                                                          "AIzaSyDGWX4OefdEbrGeqcfC2ghiWHxPMF8w1tQSourav")
                    else:

                        if tempVehicleETA > 0 and x3 > 0:
                            tempVehETA = (tempVehicleETA / x3) * x1
                        else:
                            tempVehETA = LocaAPIObj.time_calculator_without_wait([routeStatic[0], PickDropList[0]])

                    pickDropTime = LocaAPIObj.time_calculator([routeDynamic[0],routeStatic[i]])
                    tempVehETADict[x] = tempVehETA

                    if tempVehETA <= LocaAPIObj.SMObj.getValue(routeDynamic[0])["MaxServeTime"] and \
                            tempVehETA + LocaAPIObj.SMObj.getValue(PickDropList[0])["WaitTime"] \
                            + pickDropTime - tempVehicleETA <= min(max_detour + [10000]):

                        temp = list([routeDynamic[0]] + routeStatic)

                        for j in range(i + 1, min(len(routeStatic) + 2,dropIndx+2)):
                            if (tempAvailSeats + sum(len(a) for a in vehicle_matrix
                                                                     [x]['DropCount'][:j - 1]) -
                                sum(len(a) for a in vehicle_matrix[x]['PickCount'][:j - 1])) > 0 \
                                    and LocaAPIObj.time_calculator(
                                [routeDynamic[0]] + routeStatic[i:j - 1] + [routeDynamic[1]]) - \
                                    LocaAPIObj.time_calculator(routeDynamic) <= \
                                    LocaAPIObj.DTDObj.getValue("-".join(PickDropList))["MTV"]:

                                # if len(currPickList[-1]) + len(currDropList[-1]) == 0 and j == len(routeStatic) + 1:
                                #     continue

                                if len(max_detour[j - 1:]) == 0:
                                    max_detour_time = 10000
                                else:
                                    max_detour_time = min(max_detour[j - 1:])

                                temp1 = list(temp[:j] + [routeDynamic[1]] + temp[j:])
                                if LocaAPIObj.time_calculator([vehicle_matrix[x]["LastSite"]] + temp1) - LocaAPIObj.time_calculator(
                                        [vehicle_matrix[x]["LastSite"]]+routeStatic) > max_detour_time:
                                    webC.logger.debug("=============Inside-API: Vehicle %s ignored at case 7.%s" % (x,j))
                                    continue
                                else:
                                    each_poss_routes.append([vehicle_matrix[x]["LastSite"]]+temp1)
                                    crupt_Indx.append(len(each_poss_routes)-1)
                            else:
                                break


            #===============================================================================================

            for i in range(1, len(routeStatic) + 1):
                waitTime = LocaAPIObj.SMObj.getValue(routeStatic[0])["WaitTime"]
                if i == 1 and routeStatic[0] == PickDropList[0]:
                    waitTime = 0

                if waitTime + LocaAPIObj.time_calculator(routeStatic[:i] + routeDynamic[:1]) <= \
                        LocaAPIObj.SMObj.getValue(routeStatic[i - 1])["MaxServeTime"] and \
                    (tempAvailSeats + sum(len(a) for a in vehicle_matrix[x]['DropCount'][:i]) - sum(len(a) for a in vehicle_matrix[x]['PickCount'][:i])) > 0:
                    if sim == 0:
                        tempVehicleETA = LocaAPIObj.getETAUsingGAPI(x)

                    if tempVehicleETA + waitTime + LocaAPIObj.time_calculator(routeStatic[:i] + routeDynamic[:1]) <= \
                            LocaAPIObj.SMObj.getValue(routeDynamic[0])["MaxServeTime"]:

                        temp = list(routeStatic[:i] + [routeDynamic[0]] + routeStatic[i:])
                        max_detour_time = min(max_detour[i:] + [10000])
                        if LocaAPIObj.time_calculator(temp) - LocaAPIObj.time_calculator(routeStatic) > max_detour_time:
                            webC.logger.debug("=============Inside-API: Vehicle %s ignored at case 8.%s" % (x,i))
                            continue

                        for j in range(i + 1, len(routeStatic) + 2):
                            if (tempAvailSeats + sum(len(a) for a in vehicle_matrix
                                                     [x]['DropCount'][:j - 1]) -
                                sum(len(a) for a in vehicle_matrix[x]['PickCount'][:j - 1])) > 0 \
                            and LocaAPIObj.time_calculator([routeDynamic[0]]+routeStatic[i:j-1] + [routeDynamic[1]]) - \
                                    LocaAPIObj.time_calculator(routeDynamic) <= \
                                    LocaAPIObj.DTDObj.getValue("-".join(PickDropList))["MTV"]:

                                if len(max_detour[j-1:]) == 0:
                                    max_detour_time = 10000
                                else:
                                    max_detour_time = min(max_detour[j - 1:])

                                temp1 = list(temp[:j] + [routeDynamic[1]] + temp[j:])
                                if LocaAPIObj.time_calculator(temp1) - LocaAPIObj.time_calculator(routeStatic) > max_detour_time:
                                    webC.logger.debug("=============Inside-API: Vehicle %s ignored at case 8.%s.%s" % (x,i,j))
                                    continue
                                else:
                                    each_poss_routes.append(temp1)
                            else:
                                break

                    else:
                        break

                else:
                    break

        if len(each_poss_routes) == 0:
            pass
        else:
            poss_rout_dist[x] = [LocaAPIObj.time_calculator(i) - init_route_dist for i in each_poss_routes]
            for i in crupt_Indx:
                each_poss_routes[i].pop(0)
            possible_routes[x] = each_poss_routes


    webC.logger.debug("=============Inside-API: DS of possible routes duration %s" % poss_rout_dist)
    webC.logger.debug("=============Inside-API: DS of possible routes %s" % possible_routes)

    # if "V1" in possible_routes or "V18" in possible_routes:
    #     print("Testing",customerId)
    # if customerId in [79]:
    #     print("Testing")

    if len(poss_rout_dist) > 0:
        customerAssigned = True
        tempMin = 0
        tempVehicle = ""
        tempRoute = ""
        tempRouteList = []
        tempVehicleList = []
        tempMin = min(min(poss_rout_dist[i]) for i in poss_rout_dist)

        if tempMin == 0:

            for i in poss_rout_dist:
                for jIndx, j in enumerate(poss_rout_dist[i]):
                    if j == tempMin:
                        tempRoute = possible_routes[i][jIndx]

                        if tempRoute not in tempRouteList:
                            tempRouteList.append(tempRoute)
                            tempVehicleList.append(i)

            if len(tempRouteList) > 1:

                fixedVehicleCount = 0

                for i in tempVehicleList:

                    if vehicle_matrix[i]["VehicleType"] in ["static","semiDynamic","semiDynamic1"]:
                        tempVehicle = i
                        fixedVehicleCount += 1

                if fixedVehicleCount == 1:
                    tempRoute = tempRouteList[tempVehicleList.index(tempVehicle)]
                    tempRoute = removeSuccessiveDupStops(tempRoute)
                    tempAvailSeats = vehicle_matrix[tempVehicle]['SeatingCaps'] - vehicle_matrix[tempVehicle]['CurrSeated']

                    if vehicle_matrix[tempVehicle]["VehicleType"] in ["static", "semiDynamic"]:
                        pickIndx = vehicle_matrix[tempVehicle]["DropPoints"].index(PickDropList[0])
                    else:
                        pickIndx = fnd_index(LocaAPIObj.VMObj.getValue(tempVehicle)["DropPoints"],tempRoute,
                                             PickDropList,[len(z) for z in LocaAPIObj.VMObj.getValue(tempVehicle)["PickCount"]],
                                             [len(z) for z in LocaAPIObj.VMObj.getValue(tempVehicle)["DropCount"]],tempAvailSeats)[0]
                    if sim == 0:
                        tempVehicleETA = LocaAPIObj.getETAUsingGAPI(tempVehicle) + LocaAPIObj.g_time_calculator(tempRoute[:pickIndx+1])
                    else:
                        tempVehicleETA = LocaAPIObj.VMObj.getValue(tempVehicle)["ETA"] + LocaAPIObj.time_calculator(tempRoute[:pickIndx+1])

                    LocaAPIObj.VMObj.__dict__ = updateVehicleMatrix1(tempRoute, tempVehicle, vehicle_matrix,
                                                                     PickDropList, MTV,
                                                                     customerId, LocaAPIObj, tempVehETADict)
                    PickCount = LocaAPIObj.VMObj.getValue(tempVehicle)["PickCount"]
                    DropCount = LocaAPIObj.VMObj.getValue(tempVehicle)["DropCount"]
                    if pickIndx == 0 and (len(PickCount[0])+len(DropCount[0])) == 1 and tempVehicle in tempVehETADict:
                        # print(tempVehicle, tempVehETADict[tempVehicle], 1)
                        return tempVehicle, tempVehETADict[tempVehicle], 1
                    else:
                        return tempVehicle, tempVehicleETA, 0

                else:   #if only multiple dynamic or static/semi-dynamic are more than 1
                    tempRouteTime = []

                    for iIndx, i in enumerate(tempRouteList):
                        tempRouteTime.append(vehicle_matrix[tempVehicleList[iIndx]]["ETA"] + LocaAPIObj.time_calculator(i))

                    timeMin = min(tempRouteTime)

                    if len([i for i in tempRouteTime if i == timeMin]) > 1:

                        tempVehicleCaps = []

                        for i in tempVehicleList:
                            tempVehicleCaps.append(vehicle_matrix[i]["SeatingCaps"])

                        tempMinCaps = min(tempVehicleCaps)

                        tempRoute = tempRouteList[tempVehicleCaps.index(tempMinCaps)]
                        tempRoute = removeSuccessiveDupStops(tempRoute)
                        tempVehicle = tempVehicleList[tempVehicleCaps.index(tempMinCaps)]
                        tempAvailSeats = vehicle_matrix[tempVehicle]['SeatingCaps'] - vehicle_matrix[tempVehicle]['CurrSeated']

                        if vehicle_matrix[tempVehicle]["VehicleType"] in ["static", "semiDynamic"]:
                            pickIndx = vehicle_matrix[tempVehicle]["DropPoints"].index(PickDropList[0])
                        else:
                            pickIndx = fnd_index(LocaAPIObj.VMObj.getValue(tempVehicle)["DropPoints"], tempRoute,
                                                 PickDropList,
                                                 [len(z) for z in LocaAPIObj.VMObj.getValue(tempVehicle)["PickCount"]],
                                                 [len(z) for z in LocaAPIObj.VMObj.getValue(tempVehicle)["DropCount"]],
                                                 tempAvailSeats)[0]

                        if sim == 0:
                            tempVehicleETA = LocaAPIObj.getETAUsingGAPI(tempVehicle) + LocaAPIObj.g_time_calculator(
                                tempRoute[:pickIndx+1])
                        else:
                            tempVehicleETA = LocaAPIObj.VMObj.getValue(tempVehicle)["ETA"] + LocaAPIObj.time_calculator(
                                tempRoute[:pickIndx+1])

                        LocaAPIObj.VMObj.__dict__ = updateVehicleMatrix1(tempRoute, tempVehicle, vehicle_matrix,
                                                                         PickDropList, MTV,
                                                                         customerId, LocaAPIObj,tempVehETADict)
                        PickCount = LocaAPIObj.VMObj.getValue(tempVehicle)["PickCount"]
                        DropCount = LocaAPIObj.VMObj.getValue(tempVehicle)["DropCount"]
                        if pickIndx == 0 and (len(PickCount[0]) + len(DropCount[0])) == 1 and tempVehicle in tempVehETADict:
                            # print(tempVehicle, tempVehETADict[tempVehicle], 1)
                            return tempVehicle, tempVehETADict[tempVehicle], 1
                        else:
                            return tempVehicle, tempVehicleETA, 0

                    else:
                        tempRoute = tempRouteList[tempRouteTime.index(timeMin)]
                        tempRoute = removeSuccessiveDupStops(tempRoute)
                        tempVehicle = tempVehicleList[tempRouteTime.index(timeMin)]
                        tempAvailSeats = vehicle_matrix[tempVehicle]['SeatingCaps'] - vehicle_matrix[tempVehicle]['CurrSeated']

                        if vehicle_matrix[tempVehicle]["VehicleType"] in ["static", "semiDynamic"]:
                            pickIndx = vehicle_matrix[tempVehicle]["DropPoints"].index(PickDropList[0])
                        else:
                            pickIndx = fnd_index(LocaAPIObj.VMObj.getValue(tempVehicle)["DropPoints"], tempRoute,
                                                 PickDropList,
                                                 [len(z) for z in LocaAPIObj.VMObj.getValue(tempVehicle)["PickCount"]],
                                                 [len(z) for z in LocaAPIObj.VMObj.getValue(tempVehicle)["DropCount"]],
                                                 tempAvailSeats)[0]

                        if sim == 0:
                            tempVehicleETA = LocaAPIObj.getETAUsingGAPI(tempVehicle) + LocaAPIObj.g_time_calculator(
                                tempRoute[:pickIndx+1])
                        else:
                            tempVehicleETA = LocaAPIObj.VMObj.getValue(tempVehicle)["ETA"] + LocaAPIObj.time_calculator(
                                tempRoute[:pickIndx+1])

                        LocaAPIObj.VMObj.__dict__ = updateVehicleMatrix1(tempRoute, tempVehicle, vehicle_matrix,
                                                                         PickDropList, MTV,
                                                                         customerId, LocaAPIObj,tempVehETADict)
                        PickCount = LocaAPIObj.VMObj.getValue(tempVehicle)["PickCount"]
                        DropCount = LocaAPIObj.VMObj.getValue(tempVehicle)["DropCount"]
                        if pickIndx == 0 and (len(PickCount[0]) + len(DropCount[0])) == 1 and tempVehicle in tempVehETADict:
                            # print(tempVehicle, tempVehETADict[tempVehicle], 1)
                            return tempVehicle, tempVehETADict[tempVehicle], 1
                        else:
                            return tempVehicle, tempVehicleETA, 0

            else:
                tempRoute = tempRouteList[0]
                tempRoute = removeSuccessiveDupStops(tempRoute)
                tempVehicle = tempVehicleList[0]
                tempAvailSeats = vehicle_matrix[tempVehicle]['SeatingCaps'] - vehicle_matrix[tempVehicle]['CurrSeated']

                if vehicle_matrix[tempVehicle]["VehicleType"] in ["static", "semiDynamic"]:
                    pickIndx = vehicle_matrix[tempVehicle]["DropPoints"].index(PickDropList[0])
                else:
                    pickIndx = fnd_index(LocaAPIObj.VMObj.getValue(tempVehicle)["DropPoints"], tempRoute,
                                         PickDropList,
                                         [len(z) for z in LocaAPIObj.VMObj.getValue(tempVehicle)["PickCount"]],
                                         [len(z) for z in LocaAPIObj.VMObj.getValue(tempVehicle)["DropCount"]],
                                         tempAvailSeats)[0]

                if sim == 0:

                    tempVehicleETA = LocaAPIObj.getETAUsingGAPI(tempVehicle) + LocaAPIObj.g_time_calculator(tempRoute[:pickIndx+1])
                else:
                    tempVehicleETA = LocaAPIObj.VMObj.getValue(tempVehicle)["ETA"] + LocaAPIObj.time_calculator(tempRoute[:pickIndx+1])


                LocaAPIObj.VMObj.__dict__ = updateVehicleMatrix1(tempRoute, tempVehicle, vehicle_matrix,
                                                                 PickDropList, MTV,
                                                                 customerId, LocaAPIObj,tempVehETADict)
                PickCount = LocaAPIObj.VMObj.getValue(tempVehicle)["PickCount"]
                DropCount = LocaAPIObj.VMObj.getValue(tempVehicle)["DropCount"]
                if pickIndx == 0 and (len(PickCount[0]) + len(DropCount[0])) == 1 and tempVehicle in tempVehETADict:
                    # print(tempVehicle, tempVehETADict[tempVehicle], 1)
                    return tempVehicle, tempVehETADict[tempVehicle], 1
                else:
                    return tempVehicle, tempVehicleETA, 0
        else:
            tempMin += LocaAPIObj.SPObj.getValue("DetourTimeDifference")
            semiDynamicCount = 0

            for i in tempVehicleList:

                if vehicle_matrix[i]["VehicleType"] in ["semiDynamic1"]:
                    tempVehicle = i
                    semiDynamicCount += 1
            if semiDynamicCount >= 1:
                tempRoute = tempRouteList[tempVehicleList.index(tempVehicle)]
                tempRoute = removeSuccessiveDupStops(tempRoute)
                tempAvailSeats = vehicle_matrix[tempVehicle]['SeatingCaps'] - vehicle_matrix[tempVehicle]['CurrSeated']

                if vehicle_matrix[tempVehicle]["VehicleType"] in ["static", "semiDynamic"]:
                    pickIndx = vehicle_matrix[tempVehicle]["DropPoints"].index(PickDropList[0])
                else:
                    pickIndx = fnd_index(LocaAPIObj.VMObj.getValue(tempVehicle)["DropPoints"], tempRoute,
                                         PickDropList,
                                         [len(z) for z in LocaAPIObj.VMObj.getValue(tempVehicle)["PickCount"]],
                                         [len(z) for z in LocaAPIObj.VMObj.getValue(tempVehicle)["DropCount"]],
                                         tempAvailSeats)[0]
                if sim == 0:
                    tempVehicleETA = LocaAPIObj.getETAUsingGAPI(tempVehicle) + LocaAPIObj.g_time_calculator(
                        tempRoute[:pickIndx + 1])
                else:
                    tempVehicleETA = LocaAPIObj.VMObj.getValue(tempVehicle)["ETA"] + LocaAPIObj.time_calculator(
                        tempRoute[:pickIndx + 1])

                LocaAPIObj.VMObj.__dict__ = updateVehicleMatrix1(tempRoute, tempVehicle, vehicle_matrix,
                                                                 PickDropList, MTV,
                                                                 customerId, LocaAPIObj, tempVehETADict)
                PickCount = LocaAPIObj.VMObj.getValue(tempVehicle)["PickCount"]
                DropCount = LocaAPIObj.VMObj.getValue(tempVehicle)["DropCount"]
                if pickIndx == 0 and (len(PickCount[0]) + len(DropCount[0])) == 1 and tempVehicle in tempVehETADict:
                    # print(tempVehicle, tempVehETADict[tempVehicle], 1)
                    return tempVehicle, tempVehETADict[tempVehicle], 1
                else:
                    return tempVehicle, tempVehicleETA, 0
            else:
                for i in poss_rout_dist:
                    for jIndx, j in enumerate(poss_rout_dist[i]):
                        if j <= tempMin:
                            tempRoute = possible_routes[i][jIndx]

                            if tempRoute not in tempRouteList:
                                tempRouteList.append(tempRoute)
                                tempVehicleList.append(i)

                if len(tempRouteList) > 1:
                    tempSeatUtil = -1
                    for indx,i in enumerate(tempVehicleList):
                        tempSeatUtil1 = vehicle_matrix[i]["CurrSeated"] * 100 / vehicle_matrix[i]["SeatingCaps"]
                        if tempSeatUtil1 > tempSeatUtil:
                            tempSeatUtil = tempSeatUtil1
                            tempVehicle = i
                            tempRoute = tempRouteList[indx]

                    tempRoute = removeSuccessiveDupStops(tempRoute)
                    tempAvailSeats = vehicle_matrix[tempVehicle]['SeatingCaps'] - vehicle_matrix[tempVehicle]['CurrSeated']

                    if vehicle_matrix[tempVehicle]["VehicleType"] in ["static", "semiDynamic"]:
                        pickIndx = vehicle_matrix[tempVehicle]["DropPoints"].index(PickDropList[0])
                    else:
                        pickIndx = fnd_index(LocaAPIObj.VMObj.getValue(tempVehicle)["DropPoints"], tempRoute,
                                             PickDropList,
                                             [len(z) for z in LocaAPIObj.VMObj.getValue(tempVehicle)["PickCount"]],
                                             [len(z) for z in LocaAPIObj.VMObj.getValue(tempVehicle)["DropCount"]],
                                             tempAvailSeats)[0]

                    if sim == 0:
                        tempVehicleETA = LocaAPIObj.getETAUsingGAPI(tempVehicle) + LocaAPIObj.g_time_calculator(
                            tempRoute[:pickIndx + 1])
                    else:
                        tempVehicleETA = LocaAPIObj.VMObj.getValue(tempVehicle)["ETA"] + LocaAPIObj.time_calculator(
                            tempRoute[:pickIndx + 1])


                    LocaAPIObj.VMObj.__dict__ = updateVehicleMatrix1(tempRoute, tempVehicle, vehicle_matrix,
                                                                     PickDropList, MTV,
                                                                     customerId, LocaAPIObj,tempVehETADict)
                    PickCount = LocaAPIObj.VMObj.getValue(tempVehicle)["PickCount"]
                    DropCount = LocaAPIObj.VMObj.getValue(tempVehicle)["DropCount"]
                    if pickIndx == 0 and (len(PickCount[0]) + len(DropCount[0])) == 1 and tempVehicle in tempVehETADict:
                        # print(tempVehicle, tempVehETADict[tempVehicle], 1)
                        return tempVehicle, tempVehETADict[tempVehicle], 1
                    else:
                        return tempVehicle, tempVehicleETA, 0

                else:
                    tempVehicle = tempVehicleList[0]
                    tempRoute = tempRouteList[0]
                    tempRoute = removeSuccessiveDupStops(tempRoute)
                    tempAvailSeats = vehicle_matrix[tempVehicle]['SeatingCaps'] - vehicle_matrix[tempVehicle]['CurrSeated']

                    if vehicle_matrix[tempVehicle]["VehicleType"] in ["static", "semiDynamic"]:
                        pickIndx = vehicle_matrix[tempVehicle]["DropPoints"].index(PickDropList[0])
                    else:
                        pickIndx = fnd_index(LocaAPIObj.VMObj.getValue(tempVehicle)["DropPoints"], tempRoute,
                                             PickDropList,
                                             [len(z) for z in LocaAPIObj.VMObj.getValue(tempVehicle)["PickCount"]],
                                             [len(z) for z in LocaAPIObj.VMObj.getValue(tempVehicle)["DropCount"]],
                                             tempAvailSeats)[0]

                    if sim == 0:
                        tempVehicleETA = LocaAPIObj.getETAUsingGAPI(tempVehicle) + LocaAPIObj.g_time_calculator(
                            tempRoute[:pickIndx + 1])
                    else:
                        tempVehicleETA = LocaAPIObj.VMObj.getValue(tempVehicle)["ETA"] + LocaAPIObj.time_calculator(
                            tempRoute[:pickIndx + 1])


                    LocaAPIObj.VMObj.__dict__ = updateVehicleMatrix1(tempRoute, tempVehicle, vehicle_matrix,
                                                                     PickDropList, MTV,
                                                                     customerId, LocaAPIObj,tempVehETADict)
                    PickCount = LocaAPIObj.VMObj.getValue(tempVehicle)["PickCount"]
                    DropCount = LocaAPIObj.VMObj.getValue(tempVehicle)["DropCount"]
                    if pickIndx == 0 and (len(PickCount[0]) + len(DropCount[0])) == 1 and tempVehicle in tempVehETADict:
                        # print(tempVehicle, tempVehETADict[tempVehicle], 1)
                        return tempVehicle, tempVehETADict[tempVehicle], 1
                    else:
                        return tempVehicle, tempVehicleETA, 0

    return "None", "NA", 0

def removeSuccessiveDupStops(tempRoute):
    # for i in range(0, len(tempRoute)):
    #     try:
    #         tempA = tempRoute[i]
    #         tempB = tempRoute[i + 1]
    #         if tempA == tempB:
    #             del tempRoute[i + 1]
    #
    #     except IndexError as ex:
    #         pass
    for i in range(0,len(tempRoute)):
        tempA = tempRoute[i]
        if tempA != "???":
            for j in range(i + 1, len(tempRoute)):
                tempB = tempRoute[j]
                if tempA == tempB:
                    tempRoute[j] = "???"
                else:
                    break
    tempRoute = [i for i in tempRoute if i != "???"]
    return tempRoute

def fnd_index(x, y, customer, pick, drop, temp_avail_seats):
    drop_index = len(y) - 1 - y[::-1].index(customer[1])
    x = x[:drop_index]
    pickup_index = len(y[:drop_index]) - 1 - y[:drop_index][::-1].index(customer[0])
    tempA = '->'.join([str(i) for i in x])
    tempB = '->'.join([str(i) for i in y[pickup_index + 1:drop_index]])
    tempC = '->'.join([str(i) for i in y[pickup_index:drop_index]])
    if tempB in tempA:
        if len(y[pickup_index + 1:drop_index]) > 0:
            temp_list = y[pickup_index + 1:drop_index]
            tempList = tempA.replace(tempB, '???').split('->')
            idx = (len(tempList) - (tempList[::-1].index('???')) - 1) + (
                    (tempList.count('???') - 1) * (len(tempB.split('->')) - 1))
            if tempC in tempA and customer[0] == x[idx - 1]:
                temp_list = y[pickup_index:drop_index]
                tempList = tempA.replace(tempC, '???').split('->')
                idx = (len(tempList) - (tempList[::-1].index('???')) - 1) + (
                        (tempList.count('???') - 1) * (len(tempC.split('->')) - 1))

            for i in range(idx, idx + len(temp_list)):
                if temp_avail_seats + sum(drop[:i + 1]) - sum(pick[:i + 1]) < 1:
                    return fnd_index(x, y[:drop_index], customer, pick, drop, temp_avail_seats)

            return pickup_index, drop_index
        else:
            if len(x) == len(y):
                if temp_avail_seats + sum(drop[:pickup_index + 1]) - sum(pick[:pickup_index + 1]) < 1:
                    return fnd_index(x, y[:drop_index], customer, pick, drop, temp_avail_seats)
                else:
                    return pickup_index, drop_index
            elif len(x) - len(y) == 1:
                if y[:drop_index] == x[:drop_index]:
                    if temp_avail_seats + sum(drop[:drop_index]) - sum(pick[:drop_index]) < 1:
                        return fnd_index(x, y[:drop_index], customer, pick, drop, temp_avail_seats)
                    else:
                        return pickup_index, drop_index
                else:
                    if temp_avail_seats + sum(drop[:pickup_index]) - sum(pick[:pickup_index]) < 1:
                        return fnd_index(x, y[:drop_index], customer, pick, drop, temp_avail_seats)
                    else:
                        return pickup_index, drop_index
                # return pickup_index, drop_index
            else:
                return pickup_index, drop_index
    else:
        return fnd_index(x, y[:drop_index], customer, pick, drop, temp_avail_seats)

def updateVehicleMatrix1(y, tempVehicle, vehicle_matrix, customer, MTV, customerID, LocaAPIObj, tempVehETADict):
    # Sir Logic
    # if customerID in [16]:
    #     print("Testing")
    # if tempVehicle in ['V2']:
    #     print("Testing")
    # print("%s vehicle with %s route" % (tempVehicle, "->".join(y)))
    x = vehicle_matrix[tempVehicle]['DropPoints']
    drop = vehicle_matrix[tempVehicle]['DropCount']
    pickup = vehicle_matrix[tempVehicle]['PickCount']
    detour_old = vehicle_matrix[tempVehicle]['MaxDetourTime']
    # if 10000 in detour_old:
    #     detour_old[detour_old.index(10000)] = 300
    tempAvailSeats = vehicle_matrix[tempVehicle]['SeatingCaps'] - vehicle_matrix[tempVehicle]['CurrSeated']

    pickup_new = []
    drop_new = []
    detour_new = []
    # idx = fnd_index(x,y,customer, pickup, drop, tempAvailSeats)
    if vehicle_matrix[tempVehicle]["VehicleType"] in ["static", "semiDynamic"]:
        idx = vehicle_matrix[tempVehicle]["DropPoints"].index(customer[0]),vehicle_matrix[tempVehicle]["DropPoints"].index(customer[1])
    else:
        idx = fnd_index(x, y, customer, [len(i) for i in pickup], [len(i) for i in drop], tempAvailSeats)
    pickup_index = idx[0]
    drop_index = idx[1]

    # query = """update CustomerAllocation set VehicleName = '%s',
    #             PromiseTime = %s,""" % (tempVehicle, currTime +
    #                                                   vehicle_matrix[tempVehicle]["ETA"] + time_calculator(y[:pickup_index+1]))
    # query += """ ExpectedTravelTime = %s where PickupLocation = '%s' and DropLocation = '%s'
    #             and VehicleName is null and CustomerID = %s""" %(time_calculator(y[pickup_index:drop_index+1]),
    #                                                                              customer[0],customer[1],customerID)

    # SQLoad.writeOutput(query, conn)
    # conn.execute(query)
    tempMTVDiff = LocaAPIObj.time_calculator(y[pickup_index:drop_index+1]) - LocaAPIObj.time_calculator(customer)
    if len(y) - len(x) < 2:
        if customer[0] == x[pickup_index]:
            if len(y) - len(x) == 1:
                pickup_new = list(pickup[:drop_index])
                # pickup_new[pickup_index] += 1
                # pickup_new.append(0)
                pickup_new[pickup_index].append(customerID)
                pickup_new.append([])
                drop_new = list(drop[:drop_index])
                # drop_new.append(1)
                # print(customerID)
                drop_new.append([customerID])
                for i in range(drop_index, len(x)):
                    pickup_new.append(pickup[i])
                    drop_new.append(drop[i])

                if x == y[:drop_index]:
                    detour_new = detour_old + [MTV - tempMTVDiff]
                else:
                    dt = LocaAPIObj.time_calculator(y[drop_index - 1:drop_index + 2]) - LocaAPIObj.time_calculator(
                        [y[drop_index - 1], y[drop_index + 1]])
                    detour_new = list(detour_old[:drop_index])
                    detour_new.append(MTV - tempMTVDiff)

                    for j in range(drop_index, len(x)):
                        detour_new.append(detour_old[j] - dt)


            else:
                z = LocaAPIObj.SMObj.getValue(x[pickup_index])["MaxServeTime"] - \
                                      LocaAPIObj.time_calculator(x[:pickup_index + 1]) - vehicle_matrix[tempVehicle][
                                          "ETA"]
                detour_new = detour_old[:pickup_index] + [min(detour_old[pickup_index],z)] \
                             + detour_old[pickup_index+1:drop_index] + [min(detour_old[drop_index],MTV - tempMTVDiff)] \
                             + detour_old[drop_index+1:]

                if customer[1] == x[drop_index]:
                    pickup_new = copy.deepcopy(pickup)
                    # pickup_new[pickup_index] += 1
                    pickup_new[pickup_index].append(customerID)
                    drop_new = copy.deepcopy(drop)
                    # drop_new[drop_index] += 1
                    drop_new[drop_index].append(customerID)
                else:
                    pickup_new = list(pickup[:drop_index])
                    # pickup_new[pickup_index] += 1
                    # pickup_new.append(0)
                    pickup_new[pickup_index].append(customerID)
                    pickup_new.append([])
                    drop_new = list(drop[:drop_index])
                    # drop_new.append(1)
                    drop_new.append([customerID])
                    for i in range(drop_index, len(x)):
                        pickup_new.append(pickup[i])
                        drop_new.append(drop[i])

        else:
            for i in range(pickup_index):
                pickup_new.append(pickup[i])
                drop_new.append(drop[i])
            # pickup_new.append(1)
            pickup_new.append([customerID])
            # drop_new.append(0)
            drop_new.append([])
            if customer[1] == x[drop_index - 1]:
                di = x[pickup_index:].index(customer[1]) + pickup_index
                for i in range(pickup_index, len(x)):
                    pickup_new.append(pickup[i])
                    drop_new.append(drop[i])
                # drop_new[di + 1] += 1
                drop_new[di + 1].append(customerID)
                dt = LocaAPIObj.time_calculator(y[pickup_index - 1:pickup_index + 2]) - LocaAPIObj.time_calculator(
                    [y[pickup_index - 1], y[pickup_index + 1]])
                detour_new = list(detour_old[:pickup_index])
                if pickup_index == 0:
                    dt = (tempVehETADict[tempVehicle] + LocaAPIObj.SMObj.getValue(customer[0])["WaitTime"] +
                          LocaAPIObj.DTDObj.getValue("%s-%s" %(customer[0],x[0]))["Time"] - vehicle_matrix[tempVehicle]["ETA"])
                    detour_new.append(LocaAPIObj.SMObj.getValue(y[pickup_index])["MaxServeTime"] - tempVehETADict[tempVehicle])
                else:
                    detour_new.append(LocaAPIObj.SMObj.getValue(y[pickup_index])["MaxServeTime"] -
                                      LocaAPIObj.time_calculator(y[:pickup_index + 1]) - vehicle_matrix[tempVehicle][
                                          "ETA"])

                # change here for time calculator + eta
                for j in range(pickup_index, len(x)):
                    # detour_new.append(dt)
                    if x[j] == customer[1]:
                        detour_new.append(min(detour_old[j] - dt, MTV - tempMTVDiff))
                    else:
                        detour_new.append(detour_old[j] - dt)
                    # if len(pickup_new[len(detour_new)+1]) + len(pickup_new[len(detour_new)+1]) > 0 and detour_old[j] == 10000:
                    #     detour_new.append(MTV - dt)
                    # else:
                    #     detour_new.append(detour_old[j] - dt)
                    # if detour_old[j] == 10000:
                    #     detour_new.append(detour_old[j])
                    # else:
                    #     detour_new.append(detour_old[j] - dt)
            else:
                detour_new = list(detour_old)
                for i in range(pickup_index, drop_index - 1):
                    pickup_new.append(pickup[i])
                    drop_new.append(drop[i])
                # pickup_new.append(0)
                pickup_new.append([])
                # drop_new.append(1)
                drop_new.append([customerID])
                for i in range(drop_index - 1, len(x)):
                    pickup_new.append(pickup[i])
                    drop_new.append(drop[i])
    else:
        for i in range(pickup_index):
            pickup_new.append(pickup[i])
            drop_new.append(drop[i])
        # pickup_new.append(1)
        pickup_new.append([customerID])
        # drop_new.append(0)
        drop_new.append([])
        for i in range(pickup_index, drop_index - 1):
            pickup_new.append(pickup[i])
            drop_new.append(drop[i])
        # pickup_new.append(0)
        pickup_new.append([])
        # drop_new.append(1)
        drop_new.append([customerID])
        for i in range(drop_index - 1, len(x)):
            pickup_new.append(pickup[i])
            drop_new.append(drop[i])

        if len(y) == drop_index + 1:
            if len(y)  == pickup_index + 2:
                detour_new = list(detour_old + [LocaAPIObj.SMObj.getValue(y[pickup_index])["MaxServeTime"] -
                                    LocaAPIObj.SMObj.getValue(y[0])["WaitTime"] - vehicle_matrix[tempVehicle]["ETA"] -
                                                LocaAPIObj.time_calculator(y[:pickup_index+1]), MTV - tempMTVDiff])
            else:
                detour_new = list(detour_old[:pickup_index])

                dt = LocaAPIObj.time_calculator(y[pickup_index - 1:pickup_index + 2]) - \
                     LocaAPIObj.time_calculator([y[pickup_index - 1], y[pickup_index + 1]])

                if pickup_index == 0:
                    dt = (tempVehETADict[tempVehicle] + LocaAPIObj.SMObj.getValue(customer[0])["WaitTime"] +
                          LocaAPIObj.DTDObj.getValue("%s-%s" %(customer[0],x[0]))["Time"] - vehicle_matrix[tempVehicle]["ETA"])
                    detour_new.append(LocaAPIObj.SMObj.getValue(y[pickup_index])["MaxServeTime"] -
                                      tempVehETADict[tempVehicle])
                else:
                    detour_new.append(LocaAPIObj.SMObj.getValue(y[pickup_index])["MaxServeTime"] -
                                      LocaAPIObj.SMObj.getValue(y[0])["WaitTime"] -
                                      LocaAPIObj.time_calculator(y[:pickup_index + 1]) - vehicle_matrix[tempVehicle]["ETA"])

                for j in range(pickup_index, len(x)):
                    detour_new.append(detour_old[j] - dt)
                detour_new.append(MTV - tempMTVDiff)

        else:
            dt = LocaAPIObj.time_calculator(y[pickup_index - 1:pickup_index + 2]) - \
                 LocaAPIObj.time_calculator([y[pickup_index - 1], y[pickup_index + 1]])

            detour_new = list(detour_old[:pickup_index])

            if pickup_index == 0:
                dt = (tempVehETADict[tempVehicle] + LocaAPIObj.SMObj.getValue(customer[0])["WaitTime"] +
                      LocaAPIObj.DTDObj.getValue("%s-%s" % (customer[0], x[0]))["Time"] - vehicle_matrix[tempVehicle][
                          "ETA"])
                detour_new.append(LocaAPIObj.SMObj.getValue(y[pickup_index])["MaxServeTime"] -
                                  tempVehETADict[tempVehicle])
            else:
                detour_new.append(LocaAPIObj.SMObj.getValue(y[pickup_index])["MaxServeTime"]
                                  - LocaAPIObj.SMObj.getValue(y[0])["WaitTime"] -
                                  LocaAPIObj.time_calculator(y[:pickup_index + 1]) - vehicle_matrix[tempVehicle]["ETA"])


            for j in range(pickup_index, drop_index - 1):
                detour_new.append(detour_old[j] - dt)
            detour_new.append(MTV - tempMTVDiff)
            if pickup_index + 1 == drop_index:
                dt = LocaAPIObj.time_calculator(y) - LocaAPIObj.time_calculator(x)
                if pickup_index == 0:
                    # CHANGE DT CALCULATION
                    dt = (tempVehETADict[tempVehicle] + LocaAPIObj.SMObj.getValue(customer[0])["WaitTime"] +
                          LocaAPIObj.SMObj.getValue(customer[1])["WaitTime"] +
                           LocaAPIObj.DTDObj.getValue("%s-%s" % (customer[0], customer[1]))["Time"]
                          + LocaAPIObj.DTDObj.getValue("%s-%s" % (customer[1], x[0]))["Time"]
                          - vehicle_matrix[tempVehicle]["ETA"])
            else:
                dt = dt + LocaAPIObj.time_calculator(y[drop_index - 1:drop_index + 2]) - LocaAPIObj.time_calculator(
                    [y[drop_index - 1], y[drop_index + 1]])
                if pickup_index == 0:
                    dt = (tempVehETADict[tempVehicle] + LocaAPIObj.SMObj.getValue(customer[0])["WaitTime"] +
                           LocaAPIObj.DTDObj.getValue("%s-%s" % (customer[0], x[0]))["Time"] - vehicle_matrix[tempVehicle]["ETA"]) \
                         + (LocaAPIObj.time_calculator(y[1:]) - LocaAPIObj.time_calculator(x))
            for j in range(drop_index - 1, len(x)):
                detour_new.append(detour_old[j] - dt)

    # tempEmptySeats = pickDropDiff(pickup_new, drop_new, tempAvailSeats)
    if vehicle_matrix[tempVehicle]["VehicleType"] not in ["static","semiDynamic"]:
        for indx,i in enumerate(y):
            if indx > 0:
                if len(drop_new[indx]) + len(pickup_new[indx]) == 0:
                    print("New-Fix")
                    y[indx] = "????"
                    drop_new[indx] = "????"
                    pickup_new[indx] = "????"
                    detour_new[indx] = "????"

        y = [i for i in y if str(i) != "????"]
        drop_new = [i for i in drop_new if str(i) != "????"]
        pickup_new = [i for i in pickup_new if str(i) != "????"]
        detour_new = [i for i in detour_new if str(i) != "????"]

    vehicle_matrix[tempVehicle]['DropPoints'] = y
    vehicle_matrix[tempVehicle]['DropCount'] = drop_new
    vehicle_matrix[tempVehicle]['PickCount'] = pickup_new
    vehicle_matrix[tempVehicle]['MaxDetourTime'] = detour_new

    return vehicle_matrix

def pickDropDiff(pickList, DropList, totalSeats):
    try:
        diffCount = []

        for i in range(0, len(pickList)):
            diffCount.append((totalSeats + sum(DropList[0:i + 1]) - sum(pickList[0:i + 1])))

        return diffCount

    except Exception as ex:
        print("pickDropDiff-Error:%s" % ex)

def possibleRouteForSrcDst(src, dst, LocaAPIObj):
    conn = MySQLdb.connect('mysql-locatest1.cihzsxxugwmt.ap-south-1.rds.amazonaws.com',
                           'dbadmin123', 'dbpass123', 'VRPSampleNonDistributed', autocommit=True,
                           port=3306, use_unicode=True, charset="utf8")
    cursor = conn.cursor()
    try:
        directDist = LocaAPIObj.distance_calculator([src,dst])
        allPossRoute = []

        query = "Select distinct HomeStatn from vehicle"

        cursor.execute(query)

        possDestinations = [i[0] for i in cursor]

        if dst in possDestinations:

            query = "Select distinct StationName from station where StationName not in (%s,%s)"

            cursor.execute(query,(src,dst))

            allPossSites = [i[0] for i in cursor]

            tempCount = 0
            while tempCount < len(allPossSites):
                tempRoute = []
                for i in allPossSites[tempCount:]:
                    tempRoute.append(i)
                    tempAllRoute = []
                    for x in itertools.permutations(tempRoute):
                        x = list(x)
                        tempDist = LocaAPIObj.distance_calculator([src] + x + [dst])
                        if tempDist <= LocaAPIObj.SPObj.getValue("MaxRoamDistance") + directDist:
                            if [src] + tempRoute + [dst] not in allPossRoute:
                                tempAllRoute.append(tempRoute)
                                allPossRoute.append([src] + tempRoute + [dst])
                        else:
                            continue

                    if len(tempAllRoute) == 0:
                        break
                tempCount += 1
            return allPossRoute
        else:
            return []#"Destination not a HomeSite"

    except Exception as ex:
        print("Error:",ex)
    finally:
        cursor.close()
        conn.close()

def getCordsUsingETAs(srcCords,dstCords,ttlTime,ETA):
    if ttlTime == 0:
        return srcCords
    tDiff = ttlTime-ETA
    xDiff = dstCords[0]-srcCords[0]
    yDiff = dstCords[1]-srcCords[1]
    x = srcCords[0] + (xDiff*tDiff/ttlTime)
    y = srcCords[1] + (yDiff*tDiff/ttlTime)

    return x,y

def MaxRoamTimeRouteForHomeSite(vehicleId, LocaObj, currLocaPoint, env):
    conn = MySQLdb.connect('mysql-locatest1.cihzsxxugwmt.ap-south-1.rds.amazonaws.com',
                           'dbadmin123', 'dbpass123', 'VRPSampleNonDistributed', autocommit=True,
                           port=3306, use_unicode=True, charset="utf8")
    cursor = conn.cursor()
    try:
        tempVehicleDict = LocaObj.VMObj.getValue(vehicleId)

        # currLocaPoint = tempVehicleDict["DropPoints"][0]
        homeSite = tempVehicleDict["HomeSite"]
        maxRoamTime = tempVehicleDict["MaxRoamTime"]

        query = "Select PossibleRoutes from PossibleRoutes where Source = %s and Destination = %s"

        cursor.execute(query,(currLocaPoint,homeSite))
        # routes = cursor.fetchone()

        if cursor.rowcount > 0:
            routes = json.loads(cursor.fetchone()[0])
            routes.reverse()
            lenRoutes = [len(i) for i in routes]
            tempMax = max(lenRoutes)
            tempDirectTime = LocaObj.time_calculator_without_wait([currLocaPoint, homeSite])
            if len(routes) > 0:
                pastMinsCount = []
                selectedRoutes = []
                for route in routes:
                    if len(route) < tempMax-1:
                        break
                    tempRouteTime = LocaObj.time_calculator_without_wait(route)

                    if tempRouteTime-tempDirectTime <= maxRoamTime:

                        # query = """select count(*)
                        #             from  CustomerFlows cf,
                        #                     CustomerDemand cd
                        #             WHERE cd.CID = cf.CID
                        #             and cf.RequestStatus IN ('Booked', 'Processed')
                        #             AND cd.PickupPoint in %s
                        #             AND cd.DropPoint in %s
                        #             AND arrivaltime + bookedat
                        #             between %s - 900 and env.now""" %(str(tuple(route[1:len(route)-1])),
                        #                                              str(tuple(route[1:0])),env.now)

                        query1 = ""
                        route.append('????')
                        for i in range(1,len(route)-2):
                            if i == 1:
                                query1 += """ (cd.PickupPoint in ('%s')
                                            AND cd.DropPoint in %s)""" %(route[i],str(tuple(route[i+1:])))
                            else:
                                query1 += """ OR (cd.PickupPoint in ('%s')
                                            AND cd.DropPoint in %s)""" % (route[i], str(tuple(route[i+1:])))

                        route.pop()
                        query = """select count(*)
                                from CustomerFlows cf,
                                CustomerDemand cd
                                WHERE cd.CID = cf.CID
                                and cf.RequestStatus IN ('Booked', 'Processed')
                                AND (%s) 
                                AND arrivaltime + bookedat
                                between %s - 900 and %s""" %(query1,env.now,env.now)

                        cursor.execute(query)
                        tempCount = cursor.fetchone()[0]
                        if tempCount > 0:
                            pastMinsCount.append(tempCount)
                            selectedRoutes.append(route[1:])

                if len(selectedRoutes) > 0:
                    tempMax = max(pastMinsCount)
                    tempRoutes = []
                    tempRoutesTime = []
                    for indx,i in enumerate(selectedRoutes):
                        if pastMinsCount[indx] == tempMax:
                            tempRoutes.append(i)
                            tempRoutesTime.append(LocaObj.time_calculator(i))

                    if len(tempRoutes) > 1:
                        tempMin = min(tempRoutesTime)
                        tempRoute = tempRoutes[tempRoutesTime.index(tempMin)]
                        tempNextStop = tempRoute[0]
                        tempVehicleDict["DropPoints"] = tempRoute
                        tempVehicleDict["DropCount"] = [[] for i in tempRoute]
                        tempVehicleDict["PickCount"] = [[] for i in tempRoute]
                        tempVehicleDict["MaxDetourTime"] = [0 for i in tempRoute]
                        print(1,currLocaPoint,vehicleId, tempVehicleDict["DropPoints"])
                        return tempNextStop, tempVehicleDict
                    else:
                        tempNextStop = tempRoutes[0][0]
                        tempVehicleDict["DropPoints"] = tempRoutes[0]
                        tempVehicleDict["DropCount"] = [[] for i in tempRoutes[0]]
                        tempVehicleDict["PickCount"] = [[] for i in tempRoutes[0]]
                        tempVehicleDict["MaxDetourTime"] = [0 for i in tempRoutes[0]]
                        print(2,currLocaPoint,vehicleId, tempVehicleDict["DropPoints"])
                        return tempNextStop, tempVehicleDict
                else:
                    tempNextStop = homeSite
                    tempVehicleDict["DropPoints"] = [tempNextStop]
                    tempVehicleDict["DropCount"] = [[]]
                    tempVehicleDict["PickCount"] = [[]]
                    tempVehicleDict["MaxDetourTime"] = [10000]
                    # print(3,currLocaPoint,vehicleId, tempVehicleDict["DropPoints"])
                    return tempNextStop, tempVehicleDict

            else:
                tempNextStop = homeSite
                tempVehicleDict["DropPoints"] = [tempNextStop]
                tempVehicleDict["DropCount"] = [[]]
                tempVehicleDict["PickCount"] = [[]]
                tempVehicleDict["MaxDetourTime"] = [10000]
                # print(4,currLocaPoint,vehicleId, tempVehicleDict["DropPoints"])
                return tempNextStop,tempVehicleDict
        else:
            tempNextStop = homeSite
            tempVehicleDict["DropPoints"] = [tempNextStop]
            tempVehicleDict["DropCount"] = [[]]
            tempVehicleDict["PickCount"] = [[]]
            tempVehicleDict["MaxDetourTime"] = [10000]
            # print(5,currLocaPoint,vehicleId,tempVehicleDict["DropPoints"])
            return tempNextStop, tempVehicleDict
    # except Exception as ex:
    #     print("Error:",ex)
    finally:
        cursor.close()
        conn.close()

def possibleRouteForSrcDst1(src, dst, LocaAPIObj):
    conn = MySQLdb.connect('mysql-locatest1.cihzsxxugwmt.ap-south-1.rds.amazonaws.com',
                           'dbadmin123', 'dbpass123', 'VRPSampleNonDistributed', autocommit=True,
                           port=3306, use_unicode=True, charset="utf8")
    cursor = conn.cursor()
    try:
        allPossRoute = []
        query = "Select distinct HomeStatn from vehicle"

        cursor.execute(query)

        allHomeSites = [i[0] for i in cursor]

        query = "Select distinct StationName from station where StationName not in (%s,%s)"
        cursor.execute(query, (src, dst))

        possDestinations = [i[0] for i in cursor]

        IntermediateStation = []

        initial_distance = LocaAPIObj.time_calculator([src, dst])
        DetourPercentage = LocaAPIObj.SPObj.getValue("DetourPercentage")
        DetourMinTime = LocaAPIObj.SPObj.getValue("DetourMinTime")
        MaxRoamTime = max(DetourPercentage * initial_distance/100, DetourMinTime)

        for j in possDestinations:
            new_rt = [src] + [j] + [dst]
            if LocaAPIObj.time_calculator(new_rt) - initial_distance <= MaxRoamTime:
                allPossRoute.append(new_rt)
                IntermediateStation.append(j)

        for rt in allPossRoute:
            z_gen = (st for st in IntermediateStation if st not in rt)
            for i in range(1, len(rt)):
                for z in z_gen:
                    new_rt = rt[:i] + [z] + rt[i:]
                    if LocaAPIObj.time_calculator(new_rt) - initial_distance <= MaxRoamTime:
                        allPossRoute.append(new_rt)

        return allPossRoute

    except Exception as ex:
        print("Error:",ex)
    finally:
        cursor.close()
        conn.close()