from LocaRidesAPINonDistributed import webConfig as webC
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, VARCHAR, Integer, create_engine, Enum, ForeignKey, DateTime, Numeric
from sqlalchemy.orm import sessionmaker
import enum

engine = create_engine(webC.AWSDBPath)
Base = declarative_base(engine)

def loadSession():

    Session = sessionmaker(bind=engine)
    session = Session()
    return session

class BiDirectional(enum.Enum):

    nonMetroPoint = 0
    metroPoint = 1


class Station(Base):

    __tablename__ = "station"

    StationID = Column(Integer, primary_key=True, autoincrement=True)
    StationName = Column(VARCHAR(255), unique=True, nullable=False)
    # BiDirectional = Column(Enum(BiDirectional), nullable=False)
    # ImmediateReturn = Column(Integer, nullable=False)
    Latitude = Column(Numeric, nullable=False)
    Longitude = Column(Numeric, nullable=False)
    MaxServeTime = Column(Integer, nullable=False)
    WaitTime = Column(Integer, nullable=False)

    def __repr__(self):
        return """Station<StationName='%s', Latitude='%s', Longitude='%s', 
                MaxServeTime='%s', WaitTime='%s'>""" %(self.StationName, self.Latitude,
                                        self.Longitude, self.MaxServeTime, self.WaitTime)

class ServiceStatus(enum.Enum):
    outOfService = -1
    stopped = 0
    running = 1

class RoutingType(enum.Enum):
    static = 0
    semiDynamic = 1
    dynamic = 2

class Vehicle(Base):

    __tablename__ = "vehicle"

    VehicleID = Column(VARCHAR(255), primary_key=True)
    # RegNum = Column(VARCHAR(255), nullable=False)
    TtlSeats = Column(Integer, nullable=False)
    # ServStatus = Column(Enum(ServiceStatus), nullable=False)
    Status = Column(VARCHAR(10), nullable=False)
    HomeStatn = Column(VARCHAR(255), nullable=False)
    RouteType = Column(VARCHAR(15), nullable=False)
    RouteMap = Column(VARCHAR(500), nullable=True)
    MaxRoamTime = Column(Integer, nullable=False, default=300)
    # BreakStartAt = Column(Integer, nullable=True)
    # BreakEndAt = Column(Integer, nullable=True)

    def __repr__(self):
        return """Vehicle<VehicleID='%s', TtlSeats='%s', Status='%s', 
                HomeStatn='%s', RouteType='%s', MaxRoamTime='%s'>""" %(self.VehicleID, self.TtlSeats, self.Status,
                                      self.HomeStatn, self.RouteType, self.MaxRoamTime)

class DistanceTimeMatrix(Base):

    __tablename__ = "DistanceTimeMatrix"

    ID = Column(Integer, primary_key=True, autoincrement=True)
    SourceLocation = Column(VARCHAR(255), nullable=False)
    DestinationLocation = Column(VARCHAR(255), nullable=False)
    Distance = Column(Numeric(8,3), nullable=False)
    TravelTime = Column(Integer, nullable=False)
    MaxTransitVariance = Column(Integer, nullable=False, default=300)

    def __repr__(self):
        return """DistanceTimeMatrix<SourceLocation='%s', DestinationLocation='%s', 
        Distance='%s', TravelTime='%s', MaxTransitVariance='%s'>""" %(self.SourceLocation, self.DestinationLocation,
                                                                      self.Distance, self.TravelTime, self.MaxTransitVariance)

class CustomerDemand(Base):

    __tablename__ = "CustomerDemand"

    CID = Column(Integer, primary_key=True, autoincrement=True)
    ArrivalTime = Column(Integer, nullable=False)
    PickupPoint = Column(VARCHAR(50), nullable=False)
    DropPoint = Column(VARCHAR(50), nullable=False)
    # TimeToArrive = Column(Integer, nullable=False)
    CancelAt = Column(Integer,nullable=True)
    BookedAt = Column(Integer, nullable=True)
    NoShow = Column(VARCHAR(5), nullable=True)

    def __repr__(self):
        return """CustomerDemand<CID='%s', ArrivalTime='%s', PickupPoint='%s', 
        DropPoint='%s', CancelAt='%s', BookedAt='%s', NoShow='%s'>""" %(self.CID, self.ArrivalTime,
                                                                        self.PickupPoint, self.DropPoint,
                                                                        self.CancelAt, self.BookedAt, self.NoShow)

class Trip(Base):

    __tablename__ = "trip"

    TripID = Column(Integer, primary_key=True, autoincrement=True)
    VehicleID = Column(VARCHAR(255), ForeignKey(Vehicle.VehicleID, ondelete="cascade"), nullable=False)
    StartTime = Column(DateTime, nullable=False)
    SourceDepot = Column(VARCHAR(255), nullable=True)
    DestinationDepot = Column(VARCHAR(255), nullable=True)
    TotalCapacity = Column(Integer, nullable=True)
    CurrentCapacity = Column(Integer, nullable=True)
    DestinationDropPoints = Column(VARCHAR(2000), nullable=True)
    PickCount = Column(VARCHAR(100), nullable=True)
    DropCount = Column(VARCHAR(100), nullable=True)
    Distance = Column(Integer, nullable=True)
    TimeTaken = Column(Integer, nullable=True)
    InitialLocation = Column(VARCHAR(50), nullable=True)
    Message = Column(VARCHAR(255), nullable=True)

    def __repr__(self):

        return """Trip<VehicleID='%s', StartTime='%s', SourceDepot='%s', DestinationDepot='%s', 
                    TotalCapacity='%s', CurrentCapacity='%s', DestinationDropPoints='%s', 
                    PickCount='%s', DropCount='%s', Distance='%s', TimeTaken='%s', 
                    InitialLocation='%s', Message='%s'>""" %(self.VehicleID, self.StartTime, self.SourceDepot, self.DestinationDepot,
                                                             self.TotalCapacity, self.CurrentCapacity, self.DestinationDropPoints,
                                                             self.PickCount, self.DropCount, self.Distance, self.TimeTaken,
                                                             self.InitialLocation, self.Message)

class CustomerFlows(Base):

    __tablename__ = "CustomerFlows"

    ID = Column(Integer, primary_key=True, autoincrement=True)
    CID = Column(Integer, nullable=False)
    VehicleName = Column(VARCHAR(20), nullable=True)
    ExpectedTravelTime = Column(Integer, nullable=True)
    RequestStatus = Column(VARCHAR(50), nullable=True)
    ActualPickTime = Column(Integer, nullable=True)
    ActualDropTime = Column(Integer, nullable=True)
    ActualTravelTime = Column(Integer, nullable=True)
    DirectTravelTime = Column(Integer, nullable=True)
    GetETAResponse = Column(VARCHAR(50),nullable=True)
    BookAPIResponse = Column(VARCHAR(50), nullable=True)
    CancelBookingResponse = Column(VARCHAR(100), nullable=True)
    NoShowResponse = Column(VARCHAR(100), nullable=True)

    def __repr__(self):
        return """CustomerFlows<CID='%s', VehicleName='%s', ExpectedTravelTime='%s', 
        RequestStatus='%s', ActualPickTime='%s', ActualDropTime='%s', 
        ActualTravelTime='%s', DirectTravelTime='%s', 
        CancelBookingResponse='%s', NoShowResponse='%s'>""" %(self.CID, self.VehicleName, self.ExpectedTravelTime, self.RequestStatus,
                                                           self.ActualPickTime, self.ActualDropTime, self.ActualTravelTime,
                                                           self.DirectTravelTime,self.CancelBookingResponse,self.NoShowResponse)

class SetupParameters(Base):
    __tablename__ = "SetupParameters"

    ID = Column(Integer, primary_key=True, autoincrement=True)
    Parameter = Column(VARCHAR(100),unique=True,nullable=False)
    Value = Column(Integer, nullable=False)

    def __repr__(self):
        return "SetupParameters<Parameter='%s', Value='%s'>" %(self.Parameters, self.Values)

Base.metadata.create_all(engine)