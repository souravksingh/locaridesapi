from LocaRidesAPINonDistributed import LocaAPIDBTbls as apiDB, LocaMain as mainAPI, commonFunction as cf, webConfig as webC
import sqlite3, datetime as dt, math, MySQLdb, json, openpyxl, xlrd

def addNewRecord():
    session = apiDB.loadSession()
    # 28.610407, 77.037881
    newEntry = apiDB.Station(StationName="Vikar Puri", BiDirectional=apiDB.BiDirectional.nonMetroPoint,
                             ImmediateReturn=100, Latitude=28.6388,
                             Longitude=77.0738, MaxServeTime=420)

    session.add(newEntry)
    session.commit()

def addAllNewRecord():
    session = apiDB.loadSession()

    session.add_all([apiDB.Station(StationName="NSIT", BiDirectional=apiDB.BiDirectional.nonMetroPoint,
                             ImmediateReturn=100, Latitude=28.610407,
                             Longitude=77.037881, MaxServeTime=420),
                     apiDB.Station(StationName="Nawada", BiDirectional=apiDB.BiDirectional.metroPoint,
                                   ImmediateReturn=100, Latitude=28.620274,
                                   Longitude=77.045125, MaxServeTime=420),
                     apiDB.Station(StationName="Uttam Nagar West", BiDirectional=apiDB.BiDirectional.metroPoint,
                                   ImmediateReturn=100, Latitude=28.621762,
                                   Longitude=77.055821, MaxServeTime=420),
                     apiDB.Station(StationName="Mata Chanan Hospital", BiDirectional=apiDB.BiDirectional.nonMetroPoint,
                                   ImmediateReturn=100, Latitude=28.619181,
                                   Longitude=77.077869, MaxServeTime=420),
                     apiDB.Station(StationName="Dwarka Power House", BiDirectional=apiDB.BiDirectional.nonMetroPoint,
                                   ImmediateReturn=100, Latitude=28.601703,
                                   Longitude=77.070090, MaxServeTime=420),
                     apiDB.Station(StationName="Rashi Apartments", BiDirectional=apiDB.BiDirectional.nonMetroPoint,
                                   ImmediateReturn=100, Latitude=28.593864,
                                   Longitude=77.072743, MaxServeTime=420),
                     apiDB.Station(StationName="Dwarka sec-10", BiDirectional=apiDB.BiDirectional.metroPoint,
                                   ImmediateReturn=100, Latitude=28.581378,
                                   Longitude=77.056979, MaxServeTime=420),
                     apiDB.Station(StationName="Dwarka sec-11", BiDirectional=apiDB.BiDirectional.metroPoint,
                                   ImmediateReturn=100, Latitude=28.586615,
                                   Longitude=77.049110, MaxServeTime=420)
                     ])
    session.commit()

def AddRecordToSiteTable():
    connectionString = r'E:\Documents\Python\Visual Studio Python Web using Flask\VRP_SCCFlask\SupplyChainCubeFlask\SupplyChainCubeFlask\UserData\gautam\Sample.db'
    try:
        with sqlite3.connect(connectionString) as conn:

            query = "Select SiteName, SiteLatitude, SiteLongitude from Sites"
            session = apiDB.loadSession()
            addData = []
            for i in conn.execute(query):

                addData.append(apiDB.Station(StationName=str(i[0]), BiDirectional=apiDB.BiDirectional.nonMetroPoint,
                             ImmediateReturn=100, Latitude=float(i[1]),
                             Longitude=float(i[2]), MaxServeTime=420))

            session.add_all(addData)
            session.commit()
            session.close()
    except Exception as ex:
        print("Error:%s" %ex)

def AddRecordToVehicleTable():
    connectionString = r'E:\Documents\Python\Visual Studio Python Web using Flask\VRP_SCCFlask\SupplyChainCubeFlask\SupplyChainCubeFlask\UserData\gautam\Sample.db'
    try:
        with sqlite3.connect(connectionString) as conn:

            query = "Select VehicleID, 'Kt-'||VehicleName, VehicleCapacity from Vehicles"
            session = apiDB.loadSession()
            addData = []
            for i in conn.execute(query):

                addData.append(apiDB.Vehicle(VehicleID=i[0],RegNum=i[1],TtlSeats=i[2],
                                             ServStatus=apiDB.ServiceStatus.stopped,HomeStatn="Loc_10",
                                             RouteType=apiDB.RoutingType.dynamic))

            session.add_all(addData)
            session.commit()
            session.close()
    except Exception as ex:
        print("Error:%s" %ex)

def AddRecordsToDistTimMatrixTbl():
    connectionString = r'E:\Documents\Python\Visual Studio Python Web using Flask\VRP_SCCFlask\SupplyChainCubeFlask\SupplyChainCubeFlask\UserData\gautam\Sample.db'
    try:
        with sqlite3.connect(connectionString) as conn:

            query = "Select SourceLocation, DestinationLocation, Distance, TravelTime1 from DistanceTimeMatrix"
            session = apiDB.loadSession()
            addData = []
            for i in conn.execute(query):
                addData.append(apiDB.DistanceTimeMatrix(SourceLocation=i[0], DestinationLocation=i[1],
                                                        Distance=i[2], TravelTime=i[3]))

            session.add_all(addData)
            session.commit()
            session.close()
    except Exception as ex:
        print("Error:%s" % ex)

def AddRecordsToCustomerDmdTbl():
    connectionString = r'E:\Documents\Python\Visual Studio Python Web using Flask\VRP_SCCFlask\SupplyChainCubeFlask\SupplyChainCubeFlask\UserData\gautam\Sample.db'
    print("StartTime:%s" % dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S:%f"))
    try:
        with sqlite3.connect(connectionString) as conn:

            query = "Select ArrivalTime, PickupLocation, DropLocation, TimeToArrive from CustomerDemand"
            session = apiDB.loadSession()
            addData = []
            for i in conn.execute(query):
                addData.append(apiDB.CustomerDemand(ArrivalTime=i[0], PickupPoint=i[1], DropPoint=i[2], TimeToArrive=i[3]))

            session.add_all(addData)
            print("Computed At:%s" % dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S:%f"))
            session.commit()
            session.close()
            print("EndTime:%s" % dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S:%f"))
    except Exception as ex:
        print("Error:%s" % ex)

def callingLocaAPI():
    print("StartTime:%s" % dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S:%f"))
    obj = mainAPI.LocaAPI()
    print(obj.getETA("Z0P2","Z0P4",1))
    # print(obj.getPoints([28.616837, 77.031611]))
    # print(obj.getPoints([28.612597, 77.044159]))
    # print(obj.getPoints([28.596759, 77.053729]))
    # print(obj.getPoints([12.98235,77.63502],"Z0P4"))
    # print(obj.SMObj.getValue("Loc_6"))
    # print(obj.VMObj.getValue("15"))
    # print(obj.distance_calculator(["Loc_1","Loc_12"]))
    # print(cf.googleAPIDistance("12.985704,77.644637","12.971892,77.65841",1,"AIzaSyDGWX4OefdEbrGeqcfC2ghiWHxPMF8w1tQ"))
    print("EndTime:%s" % dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S:%f"))

def computeTheta():
    PC = 28.590452, 77.069498
    DC = 28.579749, 77.073360
    vehLastDestCord = 28.583235, 77.077888

    tempPD = cf.airDistance(PC[0], PC[1], DC[0], DC[1], 'M')
    tempPV = cf.airDistance(PC[0], PC[1], vehLastDestCord[0], vehLastDestCord[1], 'M')
    tempDV = cf.airDistance(vehLastDestCord[0], vehLastDestCord[1], DC[0], DC[1], 'M')
    tempEq = (pow(tempPD, 2) + pow(tempPV,2) - pow(tempDV,2)) / (2 * tempPD * tempPV)
    tempTAA = math.acos(tempEq) * 180 / math.acos(-1)
    print(tempEq)
    print(tempTAA)

def testAllPossibleRoute():
    print("StartTime:%s" % dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S:%f"))
    LocaAPIObj = mainAPI.LocaAPI()
    allPossRoute = cf.possibleRouteForSrcDst("Z0P2","Z2P33",LocaAPIObj)
    print(allPossRoute)
    print("EndTime:%s" % dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S:%f"))

def savingAllPossibleRoute():
    print("StartTime:%s" % dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S:%f"))
    conn = MySQLdb.connect('mysql-locatest1.cihzsxxugwmt.ap-south-1.rds.amazonaws.com',
                           'dbadmin123', 'dbpass123', 'VRPSampleNonDistributed', autocommit=True,
                           port=3306, use_unicode=True, charset="utf8")
    cursor = conn.cursor()
    try:
        # query = "Truncate table PossibleRoutes"
        # cursor.execute(query)
        LocaAPIObj = mainAPI.LocaAPI()
        query = """select DTM.SourceLocation, 
                            DTM.DestinationLocation 
                    from DistanceTimeMatrix DTM
                    where not exists (select 1 
                                            from PossibleRoutes PR 
                                            where PR.Source = DTM.SourceLocation 
                                            and PR.Destination = DTM.DestinationLocation)"""
        query = """select distinct sourcedepot, destinationdepot
                    from trip tr
                    where tr.InitialLocation = tr.DestinationDepot
                    and tr.CurrentCapacity = tr.TotalCapacity"""
        cursor.execute(query)
        tempData = [i for i in cursor]
        for indx,row in enumerate(tempData):

            allPossRoutes = cf.possibleRouteForSrcDst1(row[0], row[1], LocaAPIObj)
            query = "insert into PossibleRoutes(Source,Destination,PossibleRoutes) values(%s,%s,%s)"
            cursor.execute(query,(row[0],row[1],json.dumps(allPossRoutes)))
            print("row:",indx)
        print("EndTime:%s" % dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S:%f"))

    except Exception as ex:
        print("Error:",ex)
    finally:
        cursor.close()
        conn.close()

def writeOutputIntoExcelFromSQL():
    print("StartTime:%s" % dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S:%f"))
    conn = MySQLdb.connect('mysql-locatest1.cihzsxxugwmt.ap-south-1.rds.amazonaws.com',
                           'dbadmin123', 'dbpass123', 'VRPSampleNonDistributed', autocommit=True,
                           port=3306, use_unicode=True, charset="utf8")
    cursor = conn.cursor()
    try:
        query = """Select cd.CID,
                            cd.ArrivalTime,
                            cd.PickupPoint,
                            cd.DropPoint,
                            (cd.ArrivalTime+cd.BookedAt) as BookedAt,
                            Case when cd.CancelAt = 0 Then "-"
                            Else (cd.ArrivalTime+cd.BookedAt+cd.CancelAt)
                            End as CancelAt,
                            cf.VehicleName,
                            cf.ExpectedTravelTime,
                            cf.RequestStatus,
                            cf.ActualPickTime,
                            cf.ActualDropTime,
                            cf.ActualTravelTime,
                            cf.GetETAResponse,
                            cf.BookAPIResponse,
                            cf.CancelBookingResponse
                from CustomerDemand cd,
                        CustomerFlows cf
                where cd.CID = cf.CID;"""

        cursor.execute(query)

        colNames = [i[0] for i in cursor.description]
        wb = openpyxl.Workbook()
        ws = wb.active
        ws.title = "CustomerFlows"
        for indx,i in enumerate(colNames):
            ws.cell(row=1, column=indx+1, value=i)
        counter = 2
        for i in cursor:

            for jdx, j in enumerate(i):
                ws.cell(row=counter, column=jdx+1, value=j)
            counter += 1

        ws1 = wb.create_sheet("VehicleFlows")
        query = """Select * from trip"""
        cursor.execute(query)
        colNames = [i[0] for i in cursor.description]
        for indx,i in enumerate(colNames):
            ws1.cell(row=1, column=indx+1, value=i)
        counter = 2
        for i in cursor:

            for jdx, j in enumerate(i):
                ws1.cell(row=counter, column=jdx+1, value=j)
            counter += 1

        wb.save(webC.excelOutput)


    finally:
        wb.close()
        cursor.close()
        conn.close()
        print("EndTime:%s" % dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S:%f"))

def testingGetCordsUsingETAs():
    src = 28.596612, 77.050615
    dst = 28.587079, 77.065163
    ttlTime = 23*60
    ETA = 15*60
    newCords = cf.getCordsUsingETAs(src,dst,ttlTime,ETA)
    print(newCords)

def importDataIntoMySQLFrmExcelSiteTbl():
    '''Import data into Site table'''
    conn = MySQLdb.connect('mysql-locatest1.cihzsxxugwmt.ap-south-1.rds.amazonaws.com',
                           'dbadmin123', 'dbpass123', 'VRPSampleNonDistributed',
                           port=3306, use_unicode=True, charset="utf8")
    cursor = conn.cursor()
    try:
        excelFile = webC.excelInputPath

        cursor.execute("Truncate table station")

        with xlrd.open_workbook(excelFile) as wb:

            for i in wb.sheet_names():

                sheet = wb.sheet_by_name(i)
                # print("Got Sheet:%s" % i)
                if i == "Site":
                    print("Reading Sheet:%s" % i)
                    for j in range(sheet.nrows):

                        if j > 0:
                            tempData = []
                            for k in range(sheet.ncols):

                                tempData.append(sheet.cell_value(j, k))
                            query = """insert into station(StationName, Latitude, Longitude, MaxServeTime, WaitTime)
                                                    values(%s,%s,%s,%s,%s)"""
                            cursor.execute(query, tuple(tempData))
                            print(j)


        print("===========The End===============")

    # except Exception as ex:
    #     print("importDataIntoMySQLFrmExcelSiteTbl-Error:%s" %ex)
    finally:
        conn.commit()
        cursor.close()
        conn.close()

# importDataIntoMySQLFrmExcelSiteTbl()
# writeOutputIntoExcelFromSQL()
# testingGetCordsUsingETAs()
# computeTheta()
callingLocaAPI()
# addNewRecord()
# addAllNewRecord()
# AddRecordToSiteTable()
# AddRecordToVehicleTable()
# AddRecordsToDistTimMatrixTbl()
# AddRecordsToCustomerDmdTbl()
# testAllPossibleRoute()
# savingAllPossibleRoute()
