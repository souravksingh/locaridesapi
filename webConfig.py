import os, logging
from sqlalchemy.engine.url import URL

RootPath = r'E:\Documents\Python\Python Coding Practice\LocaRidesAPINonDistributed'

excelInputPath = os.path.join(RootPath,"Input.xlsx")

LocaDBPath = "sqlite:///%s" %os.path.join(RootPath,'LocaRides.db')

TempDB = "sqlite:///%s" %r'E:\Documents\Python\Python Coding Practice\Temp\App_Data\sample.db'

AWSDBPath = URL ('mysql',
                host = 'mysql-locatest1.cihzsxxugwmt.ap-south-1.rds.amazonaws.com',
                username = 'dbadmin123',
                password = 'dbpass123',
                database = 'VRPSampleNonDistributed')

ETARefreshVal = 15

logFilePath = os.path.join(RootPath,'API.log')

if os.path.exists(logFilePath):
    os.remove(logFilePath)

excelOutput = os.path.join(RootPath,'output.xlsx')
logging.basicConfig(filename=logFilePath,level=logging.DEBUG)
logger = logging.getLogger('LocaAPI')