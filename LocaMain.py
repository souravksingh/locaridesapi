from LocaRidesAPINonDistributed import LocaAPIDBTbls as apiDB, commonFunction as cf, \
    DataStructureClasses as DSC, loca_sqlalchemy as appDB, webConfig as webC
import json


class LocaAPI:

    SMObj = DSC.Site_Matrix()
    VMObj = DSC.Vehicle_Matrix()
    DTDObj = DSC.DistanceTimeData()
    SPObj = DSC.SetupParameters()

    def __init__(self):

        session = apiDB.loadSession()

        # tempData = session.query(apiDB.Station).all()
        tempData = session.query(apiDB.Station).all()

        for row in tempData:
            tempdict = {"WaitTime":row.WaitTime, "MaxServeTime":row.MaxServeTime, "PastPickup": {},
                        "SiteLat":row.Latitude, "SiteLong":row.Longitude}
            self.SMObj.setValue(row.StationName,tempdict)

        tempData = session.query(apiDB.Vehicle).all()

        for row in tempData:
            tempCords = self.SMObj.getValue(row.HomeStatn)["SiteLat"], self.SMObj.getValue(row.HomeStatn)["SiteLong"]

            tempdict = {"SeatingCaps": row.TtlSeats, "CurrSeated": 0, "HomeSite": row.HomeStatn,
                        "DropCount": [[]], "PickCount": [[]], "MaxDetourTime": [10000], "ETA": 0,
                        "MaxRoamTime": row.MaxRoamTime, "VehicleType": row.RouteType, "Status": "Active",
                        "Cords": tempCords, "LastSite": row.HomeStatn}

            if row.RouteType in ["static","semiDynamic"]:

                tempdict["DropPoints"] = json.loads(row.RouteMap)
                tempdict["DropCount"] = tempdict["PickCount"] = [[] for i in tempdict["DropPoints"]]
                tempdict["MaxDetourTime"] = [0 for i in tempdict["DropPoints"]]
            elif row.RouteType in ["semiDynamic1"]:
                tempdict["PossibleDrops"] = json.loads(row.RouteMap)
                tempdict["DropPoints"] = [row.HomeStatn]

            else:
                tempdict["DropPoints"] = [row.HomeStatn]

            self.VMObj.setValue(row.VehicleID, tempdict)

        tempData = session.query(apiDB.SetupParameters).all()

        for row in tempData:
            key = row.Parameter
            value = row.Value
            self.SPObj.setValue(key, value)

        tempData = session.query(apiDB.DistanceTimeMatrix).all()

        for row in tempData:
            key = "%s-%s" %(row.SourceLocation, row.DestinationLocation)
            tempdict = {"Distance":float(row.Distance), "Time":row.TravelTime,
                        "MTV": row.MaxTransitVariance}
            self.DTDObj.setValue(key,tempdict)

        session.close()

    def distance_calculator(self,distList):
        """

        :param distList:This input is of type List of size more than 1 containing different loca points as elements
        :return: It will return the total distance of the provided route.
        """
        count = 1
        rout_dist = 0
        for j in distList:
            if count == len(distList):
                break
            else:
                rout_dist += self.DTDObj.getValue('%s-%s' % (distList[count - 1], distList[count]))["Distance"]
                count += 1

        return rout_dist

    def time_calculator(self,distList1):
        """

        :param distList1:This input is of type List of size more than 1 containing different loca points as elements
        :return: This will return the total time required to travel through the provided route.
        """
        distList = list(distList1)
        distList = cf.removeSuccessiveDupStops(distList)
        count = 1
        rout_dist = 0
        for j in distList:
            if count == len(distList):
                break
            else:
                if distList[count - 1] == distList[count]:
                    wait_time = 0
                elif (count-1) > 0:
                    wait_time = self.SMObj.getValue(distList[count - 1])["WaitTime"]
                else:
                    wait_time = 0
                rout_dist += self.DTDObj.getValue('%s-%s' % (distList[count - 1], distList[count]))["Time"] + wait_time
                count += 1
        return rout_dist

    def time_calculator_without_wait(self,distList):
        """

        :param distList:This input is of type List of size more than 1 containing different loca points as elements
        :return: This will return the total time required to travel through the provided route.
        """
        count = 1
        rout_dist = 0
        for j in distList:
            if count == len(distList):
                break
            else:
                if distList[count - 1] == distList[count]:
                    wait_time = 0
                else:
                    wait_time = 0
                rout_dist += self.DTDObj.getValue('%s-%s' % (distList[count - 1], distList[count]))["Time"] + wait_time
                count += 1
        return rout_dist

    def getVehicleCurrCords(self,vehicleID, sim=0):
        """

        :param vehicleID:
        :param sim:
        :return:
        """
        if sim==0:
            return appDB.Vehicle.get_vehicle_location(vehicleID)
        elif sim == 1:
            return self.VMObj.getValue(vehicleID)["Cords"]

    def getETAUsingGAPI(self,vehicleID):
        """

        :param vehicleID: This is a unique vehicle Id
        :return:This will return the ETA for the intransit vehicle to reach its next stop.
        """

        src = ",".join(appDB.Vehicle.get_vehicle_location(vehicleID))
        dstProp = self.SMObj.getValue(self.VMObj.getValue(vehicleID)["DropPoints"][0])
        dst = "%s,%s" %(dstProp["SiteLat"],dstProp["SiteLong"])

        ETA = cf.googleAPITimeGap(src,dst,1,"AIzaSyDGWX4OefdEbrGeqcfC2ghiWHxPMF8w1tQSourav")

        return ETA

    def g_time_calculator(self,distList):
        """

        :param distList:This input is of type List of size more than 1 containing different loca points as elements
        :return:This will return the total time required to travel through the provided route using GoogleMaps API.
        """
        count = 1
        rout_dist = 0
        for j in distList:
            if count == len(distList):
                break
            else:
                if distList[count - 1] == distList[count]:
                    wait_time = 0
                else:
                    wait_time = self.SMObj.getValue(distList[count - 1])["WaitTime"]
                srcdict = self.SMObj.getValue(distList[count - 1])
                dstdict = self.SMObj.getValue(distList[count])
                rout_dist += cf.googleAPITimeGap("%s,%s" % (srcdict["SiteLat"], srcdict["SiteLong"]),
                                                 "%s,%s" % (dstdict["SiteLat"], dstdict["SiteLong"]), 1,
                                                 "AIzaSyDGWX4OefdEbrGeqcfC2ghiWHxPMF8w1tQSourav") + wait_time
                count += 1
        return rout_dist

    def getPoints(self,currLoc,destination,APIType=0,gKey=""):
        """
        :param currLoc: This parameter is of type list having 2 elements
                        that is latitude and longitude of customer's current position.
        :param destination: This parameter is of type String which is a name of a locapoint
        :param APIType: Here, APIType = 0 means using Air-distance api and
                        APIType = 1 means using google distance matrix api
                        to get the nearest Loca points
        :param gKey: If APIType = 1 then google API key is required to complete the process.
        :return:  This method will return List of dictionary elements with attributes as
                    Name(name of the loca point) and distance(distance from customer to that point)
        """
        resultList = []
        withinDist = self.SPObj.getValue("ThresholdDistanceGetPoints")
        if APIType == 0:

            for lp in self.SMObj.__dict__.keys():
                tempName = lp
                tempLat = float(self.SMObj.getValue(lp)["SiteLat"])
                tempLong = float(self.SMObj.getValue(lp)["SiteLong"])

                tempDist = cf.airDistance(currLoc[0], currLoc[1], tempLat, tempLong, "M")

                if tempDist <= withinDist and tempName != destination:  # and len(resultList) <= 3:
                    resultList.append({"Name": tempName, "Distance": tempDist})

            if len(resultList) == 0:
                resultList.append({"Message": "Sorry, No Loca Points near you"})
            else:

                for indx, ival in enumerate(resultList):
                    tempDist = ival["Distance"]
                    for j in range(indx + 1, len(resultList)):

                        if tempDist > resultList[j]["Distance"]:
                            tempDist = resultList[j]["Distance"]
                            tempDict = resultList[j]
                            resultList[j] = resultList[indx]
                            resultList[indx] = tempDict



        else:
            if len(gKey) == 0:
                resultList.append({"Message": "Please provide google API key to proceed"})
                return resultList
            tempSrc = ",".join(currLoc)
            for lp in self.SMObj.__dict__.keys():
                tempName = lp
                tempDistination= "%s,%s" %(float(self.SMObj.getValue(lp)["SiteLat"]),
                                           float(self.SMObj.getValue(lp)["SiteLong"]))

                tempDist = cf.googleAPIDistance(tempSrc,tempDistination,1,gKey)

                if tempDist not in ["ZERO_RESULTS","OVER_QUERY_LIMIT"]:

                    if tempDist <= withinDist and tempName != destination:  # and len(resultList) <= 3:
                        resultList.append({"Name": tempName, "Distance": tempDist})

            if len(resultList) == 0:
                resultList.append({"Message": "Sorry, No Loca Points near you"})
            else:

                for indx, ival in enumerate(resultList):

                    for j in range(indx + 1, len(resultList)):

                        if ival["Distance"] > resultList[j]["Distance"]:
                            tempDict = resultList[j]
                            resultList[j] = resultList[indx]
                            resultList[indx] = tempDict

        if "Message" not in resultList[0].keys():
            tempTime = []
            for indx,i in enumerate(resultList):
                i["Time"] = int(self.DTDObj.getValue("%s-%s" %(i["Name"],destination))["Time"])
                tempTime.append(i["Time"])


            tempMin = min(tempTime) + max(self.SPObj.getValue("ThresholdPctDestinationPoint")*min(tempTime)/100,
                                          self.SPObj.getValue("ThresholdTimeDestinationPoint"))

            resultList = [{'Name': i["Name"], 'Distance': i["Distance"]} for i in resultList if i["Time"] <= tempMin]

        return resultList

    def getETA(self,source,destination,sim=0):
        """
        :param source: This will be of type String, Loca point name choosen by customer to avail pick up service
        :param destination: This will be of type String, Loca point name choosen by customer to get dropped
        :param sim: (optional) This parameter is set to 1, when running simulation
        :return: selected vehicleID and ETA to reach pick point
        """

        ETA = cf.getETAMethod([source,destination],self,sim=sim)

        return ETA

    def BookVehicle(self,source, destination, customerID, sim=0):
        """

        :param source: This will be of type String, Loca point name choosen by customer to avail pick up service.
        :param destination: This will be of type String, Loca point name choosen by customer to get dropped.
        :param customerID: This will be of type int.
        :param sim: (optional) This parameter is set to 1, when running simulation
        :return: This will return VehicleID (which could be different from what expected) and ETA to reach the pickup point
                and DirectPickupFlag (if this is 1 then driver is required to be notified for changing his current next drop point)
        """

        vehicleID,ETA,DirectPickupFlag = cf.assignVehicle([source,destination],self,customerID,sim=sim)

        if DirectPickupFlag > 0:
            if ETA < webC.ETARefreshVal:
                ETA = webC.ETARefreshVal
            tempDict = self.VMObj.getValue(vehicleID)
            tempDict["ETA"] = ETA
            self.VMObj.setValue(vehicleID,tempDict)

        return vehicleID, ETA,DirectPickupFlag

    def cancelBooking(self,customerID, vehicleID):
        """

        :param customerID:This will be of type int.
        :param vehicleID:This will be of type int. the id of the vehicle that is alloted to the respective customer
        :return:This method will return a text confirming the cancellation is done on successfull execution.
        """

        tempVehicleDict = self.VMObj.getValue(vehicleID)
        tempDropsList = tempVehicleDict["DropPoints"]
        tempPCList = tempVehicleDict["PickCount"]
        tempDCList = tempVehicleDict["DropCount"]
        tempMDTList = tempVehicleDict["MaxDetourTime"]
        zIndx = 0
        isSuccess = False
        for indx,i in enumerate(tempPCList):

            if customerID in i:
                zIndx = indx
                tempPCList[indx].pop(tempPCList[indx].index(customerID))
                isSuccess = True
                break
                # if len(i) == 1:
                #     del tempPCList[indx]
                #     del tempDropsList[indx]
                # else:
                #     del tempPCList[indx][tempPCList[indx].index(customerID)]

        for indx,i in enumerate(tempDCList[zIndx:]):

            if customerID in i:
                tempDCList[indx+zIndx].pop(tempDCList[indx+zIndx].index(customerID))
                isSuccess = True
                # if len(i) == 1:
                #     del tempDCList[indx]
                #     del tempDropsList[indx]
                # else:
                #     del tempDCList[indx][tempDCList[indx].index(customerID)]
        if str(tempVehicleDict["VehicleType"]).upper() == "DYNAMIC":
            for indx,i in enumerate(tempDropsList):

                if len(tempPCList[indx]) == 0 and len(tempDCList[indx]) == 0:
                    tempDropsList[indx] = "???"
                    tempPCList[indx] = "???"
                    tempDCList[indx] = "???"
                    tempMDTList[indx] = "???"

        tempVehicleDict["DropPoints"] = [i for i in tempDropsList if i != "???"]
        tempVehicleDict["PickCount"] = [i for i in tempPCList if i != "???"]
        tempVehicleDict["DropCount"] = [i for i in tempDCList if i != "???"]
        tempVehicleDict["MaxDetourTime"] = [i for i in tempMDTList if i != "???"]

        self.VMObj.setValue(vehicleID,tempVehicleDict)
        if isSuccess:
            return "Booking Cancelled"
        else:
            return "Customer doesn't exist"

    def NoShow(self,customerID, vehicleID):

        """
        :param customerID: This will be of type int.The id of the customer that
                            was supposed to be the current pickup.
        :param vehicleID:This will be of type int.
        :return: This method will return a text confirming that the route have been
                    modified and the respective customer have been dropped from the vehicle scope.
        """
        # tempVehicleDict = self.VMObj.getValue(vehicleID)
        # tempDropsList = tempVehicleDict["DropPoints"]
        # tempPCList = tempVehicleDict["PickCount"]
        # tempDCList = tempVehicleDict["DropCount"]
        #
        # for indx, i in enumerate(tempPCList):
        #
        #     if customerID in i:
        #         if len(i) == 1:
        #             del tempPCList[indx]
        #             del tempDropsList[indx]
        #         else:
        #             del tempPCList[indx][tempPCList[indx].index(customerID)]
        #
        # for indx, i in enumerate(tempDCList):
        #
        #     if customerID in i:
        #         if len(i) == 1:
        #             del tempDCList[indx]
        #             del tempDropsList[indx]
        #         else:
        #             del tempDCList[indx][tempDCList[indx].index(customerID)]
        #
        # self.VMObj.setValue(vehicleID, tempVehicleDict)

        tempVehicleDict = self.VMObj.getValue(vehicleID)
        tempDropsList = tempVehicleDict["DropPoints"]
        tempPCList = tempVehicleDict["PickCount"]
        tempDCList = tempVehicleDict["DropCount"]
        tempMDTList = tempVehicleDict["MaxDetourTime"]
        zIndx = 0
        isSuccess = False
        for indx, i in enumerate(tempPCList):

            if customerID in i:
                zIndx = indx
                tempPCList[indx].pop(tempPCList[indx].index(customerID))
                isSuccess = True
                # if len(i) == 1:
                #     del tempPCList[indx]
                #     del tempDropsList[indx]
                # else:
                #     del tempPCList[indx][tempPCList[indx].index(customerID)]

        for indx, i in enumerate(tempDCList[zIndx:]):

            if customerID in i:
                tempDCList[indx].pop(tempDCList[indx].index(customerID))
                isSuccess = True
                # if len(i) == 1:
                #     del tempDCList[indx]
                #     del tempDropsList[indx]
                # else:
                #     del tempDCList[indx][tempDCList[indx].index(customerID)]

        if str(tempVehicleDict["VehicleType"]).upper() == "DYNAMIC":
            for indx, i in enumerate(tempDropsList):

                if len(tempPCList[indx]) == 0 and len(tempDCList[indx]) == 0:
                    tempDropsList[indx] = "???"
                    tempPCList[indx] = "???"
                    tempDCList[indx] = "???"
                    tempMDTList[indx] = "???"

        tempVehicleDict["DropPoints"] = [i for i in tempDropsList if i != "???"]
        tempVehicleDict["PickCount"] = [i for i in tempPCList if i != "???"]
        tempVehicleDict["DropCount"] = [i for i in tempDCList if i != "???"]
        tempVehicleDict["MaxDetourTime"] = [i for i in tempMDTList if i != "???"]

        self.VMObj.setValue(vehicleID, tempVehicleDict)
        if isSuccess:
            return "Route updated"
        else:
            return "Customer doesn't exist"

    def DriverBreakStart(self, vehicleID):

        """

        :param vehicleID:This will be of type int. ID of the vehicle that wants to go for a break.
        :return:This method will return a text confirming that the vehicle will no longer get any
                service request till it's break is over.
        """
        tempVehicleDict = self.VMObj.getValue(vehicleID)
        tempVehicleDict["Status"] = "Inactive"
        self.VMObj.setValue(vehicleID,tempVehicleDict)

        return "Your break will start soon"

    def TripStart(self, vehicleID):

        """

        :param vehicleID:This will be of type int. ID of the vehicle that wants to go for a break.
        :return:This method will return a text confirming that the vehicle can start for the day.
        """
        tempVehicleDict = self.VMObj.getValue(vehicleID)
        tempVehicleDict["Status"] = "active"
        self.VMObj.setValue(vehicleID,tempVehicleDict)

        return "Welcome to Loca Rides"

    def TripEnd(self, vehicleID):
        """

        :param vehicleID: This will be of type int. ID of the vehicle that wants to go for a break.
        :return:This method will return a text confirming that the vehicle is done the day.
        """
        tempVehicleDict = self.VMObj.getValue(vehicleID)
        tempVehicleDict["Status"] = "outofservice"
        self.VMObj.setValue(vehicleID, tempVehicleDict)

        return "See you soon"

    def DriverBreadEnd(self, vehicleID, currLocaPoint):
        """

        :param vehicleID: This will be of type int. ID of the vehicle that wants to start
                            getting customer service request.
        :param currLocaPoint: This will be of type String. the current loca point name
                            from where the vehicle wants to start being available.
        :return:This method will return a text confirming that the vehicle will start getting
                service requests.
        """
        tempVehicleDict = self.VMObj.getValue(vehicleID)
        tempVehicleDict["Status"] = "Active"
        if tempVehicleDict["VehicleType"] == "dynamic":
            tempVehicleDict["DropPoints"].append(currLocaPoint)
            tempVehicleDict["DropCount"] = [[]]
            tempVehicleDict["PickCount"] = [[]]
            tempVehicleDict["MaxDetourTime"] = [10000]

        else:
            if currLocaPoint in tempVehicleDict["DropPoints"]:
                tempIndx = tempVehicleDict["DropPoints"].index(currLocaPoint)
                tempVehicleDict["DropPoints"] = tempVehicleDict["DropPoints"][tempIndx:] + tempVehicleDict["DropPoints"][:tempIndx]
                tempVehicleDict["DropCount"] = tempVehicleDict["DropCount"][tempIndx:] + tempVehicleDict["DropCount"][:tempIndx]
                tempVehicleDict["PickCount"] = tempVehicleDict["PickCount"][tempIndx:] + tempVehicleDict["PickCount"][:tempIndx]
                tempVehicleDict["MaxDetourTime"] = tempVehicleDict["MaxDetourTime"][tempIndx:] + \
                                                   tempVehicleDict["MaxDetourTime"][:tempIndx]
            else:
                raise cf.LocaAPIError("Error:This locapoint is not associated with your vehicle")
        # tempVehicleDict["HomeSite"] = currLocaPoint
        self.VMObj.setValue(vehicleID,tempVehicleDict)

        return "Welcome back"

    def GetNextStop(self,vehicleID, currLocaPoint, env):
        """

        :param vehicleID: This will be of type int. ID of the vehicle requesting for next loca point.
        :param currLocaPoint:This will be of type String. Name of the current loca point where
                vehicle have arrived or leaving.
        :return:This method will return loca point name where vehicle is required to go next.
        """
        tempVehicleDict = self.VMObj.getValue(vehicleID)
        tempVehicleDict["LastSite"] = currLocaPoint
        if vehicleID in ["V18"]:
            print(currLocaPoint.upper(),"==",str(tempVehicleDict["DropPoints"][0]).upper()," at ",env.now)
        if currLocaPoint.upper() == str(tempVehicleDict["DropPoints"][0]).upper():
            tempVehicleDict["CurrSeated"] += (len(tempVehicleDict["PickCount"][0])-len(tempVehicleDict["DropCount"][0]))
            tempVehicleDict["DropPoints"].pop(0)
            tempVehicleDict["DropCount"].pop(0)
            tempVehicleDict["PickCount"].pop(0)
            tempVehicleDict["MaxDetourTime"].pop(0)

            if len(tempVehicleDict["DropPoints"]) == 0:
                if tempVehicleDict["Status"] == "Active":

                    tempNextStop, tempVehicleDict = cf.MaxRoamTimeRouteForHomeSite(vehicleID,self,currLocaPoint,env)

                    # tempNextStop = tempVehicleDict["HomeSite"]
                    # tempVehicleDict["DropPoints"] = [tempNextStop]
                    # tempVehicleDict["DropCount"] = [[]]
                    # tempVehicleDict["PickCount"] = [[]]
                    # tempVehicleDict["MaxDetourTime"] = [10000]

                else:
                    return "You can have your break now"
            else:
                tempNextStop = tempVehicleDict["DropPoints"][0]

                if tempVehicleDict["VehicleType"] in ["static","semiDynamic"] and \
                    tempVehicleDict["Status"] == "Inactive" and \
                    sum([len(i) for i in tempVehicleDict["DropCount"]]) == 0 and \
                    sum([len(i) for i in tempVehicleDict["PickCount"]]) == 0:
                    return "You can have your break now"

                elif tempVehicleDict["VehicleType"] in ["static","semiDynamic"]:
                    tempVehicleDict["DropPoints"].append(currLocaPoint)
                    tempVehicleDict["DropCount"].append([])
                    tempVehicleDict["PickCount"].append([])
                    tempVehicleDict["MaxDetourTime"].append(10000)

                if tempVehicleDict["VehicleType"] in ["semiDynamic","semiDynamic1"]:
                    for z in range(0,len(tempVehicleDict["DropPoints"])):
                        if len(tempVehicleDict["PickCount"][z]) > 0 or len(tempVehicleDict["DropCount"][z]) > 0:
                            tempNextStop = tempVehicleDict["DropPoints"][z]
                            tempVehicleDict["DropPoints"] = tempVehicleDict["DropPoints"][z:] + tempVehicleDict["DropPoints"][:z]
                            tempVehicleDict["DropCount"] = tempVehicleDict["DropCount"][z:] + tempVehicleDict["DropCount"][:z]
                            tempVehicleDict["PickCount"] = tempVehicleDict["PickCount"][z:] + tempVehicleDict["PickCount"][:z]
                            tempVehicleDict["MaxDetourTime"] = tempVehicleDict["MaxDetourTime"][z:] + \
                                                               tempVehicleDict["MaxDetourTime"][:z]
                            break

            self.VMObj.setValue(vehicleID, tempVehicleDict)
            return tempNextStop
        else:
            raise cf.LocaAPIError("Vehicle %s travelling beyond(%s != %s) the expected route" %(vehicleID,
                                                                                                currLocaPoint,
                                                                                                tempVehicleDict[
                                                                                                    "DropPoints"][0]))
